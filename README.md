# R5.10 - Nouveau paradigme de bdd

## Projet Punto

### Table des matières

### Installation

#### Prérequis

- Avoir docker d'installé
- Avoir npm d'installé
- Avoir maven d'installé (ce qui inclus d'avoir un java 17 minimum d'installé)

#### Procedure

- Se mettre dans le repertoire `punto`
- Lancer le frontend
    ```shell
    cd backend && mvn clean install && mvn spring-boot:run && cd .. 
    ```
- Lancer le backend
  ```shell
  cd frontend && npm install && npm run serve && cd .. 
  ```
- Lancer les bases de données
    ```shell
    cd databases && docker-compose up -d
    ```

### Technologies utilisées

#### Architecture du projet

J'ai décidé d'articuler mon projet autour de l'architecture MVC.
Pour cela, mon projet se décompose en 2 parties : le Backend (api, gestion du jeu et connection vers les bases de
données) et le Frontend (interface utilisateur, administration des bdds).

#### Backend

Pour la partie backend, j'ai choisi le langage de programmation Java et le framework Spring Boot, comme c'est ce que
je connais le mieux.
J'ai utilisé le framework Spring Boot pour sa simplicité de mise en place et sa facilité d'utilisation.
De plus, l'ajout d dépendances est très simple et permet de gagner beaucoup de temps.

#### Frontend

Pour la partie frontend, j'ai choisi le langage de programmation Javascript et le framework Vue.js.
J'ai choisi Vue.js car c'est un framework que je connais bien et que j'apprécie beaucoup.
De plus, il est très simple à mettre en place et à utiliser.

#### Base de données

Pour les bases de données, j'ai choisi celles qui étaient imposées, à savoir :

- MongoDB
- MySql
- SqLite3

J'ai choisi de dockeriser les bases de données mysql et mongoDb pour faciliter le déploiement de l'application.

#### Patrons de conception utilisés

J'ai pu appliquer plusieurs patrons de conception dans mon projet, principalement du côté du backend :

- Singletons: pour les classes de connection aux bases de données et la partie `service` du backend
- Factory: pour la création des objets de connection aux bases de données

Les diagrames ed classes UML sont disponibles dans le repertoire `doc` du projet.
Vous pouvez les consulter dans la partie `Annexes` de ce document.

#### Outil de gestion de projet
Pour gérer mon projet, j'ai utilisé le gestionnaire de version Git et l'outil de gestion de projet Gitlab.
J'ai décomposé mon projet en plusieurs branches :
- Main: branche principale du projet
- Dev: branche de développement
- FEATURE-xxx: branches de fonctionnalités

### Structure du projet

1. Repertoire `backend`: contient le code source du backend On retrouve dedans une structure de projet `clasique` pour un 
projet Spring Boot utilisant maven.
  Dans le repertoire `src/main/java`, on retrouve les packages suivants :
   - `com.punto`: contient la classe `Application` qui est la classe principale du projet ainsi que les fichiers de configurations
   - `com.punto.controller`: contient les classes de controleurs du projet (API)
   - `com.punto.dto`: contient les classes de DTO (Data Transfer Object) du projet
   - `com.punto.model`: contient les classes de modèles du projet (Représentation objet du jeu)
   - `com.punto.database`: contient la Factory et les implementations des classes de connection aux bases de données
   - `com.punto.service`: contient la classe de service du projet. C'est grace a cette classe que les différentes parties 
   du model peuvent communiquer entre elles

2. Repertoire `frontend`: contient le code source du frontend. On retrouve dedans une structure de projet `clasique` pour un
projet Vue.js utilisant npm.
  Dans le repertoire `src`, on retrouve les sous repertoires suivants :
    - `assets`: contient les fichiers statiques du projet (images, ...)
    - `components`: contient les composants du projet
    - `router`: contient les routes du projet
    - `store`: contient les stores du projet
    - `views`: contient les vues du projet
    - `App.vue`: composant principal du projet
    - `main.js`: point d'entrée du projet

3. Repertoire `databases`: contient les fichiers de configuration des bases de données et les repertoires de stockage des sauvegardes


### Fonctionnalitées implementées
Les screens du jeu peuvent être retrouvés dans les annexes de ce document.
Voici la liste des fonctionnalitées implémentées :

#### Jeu
- Jeu a 2 joueurs
- Grille mobile et réactive
- Detection des gagnants
- Gestion des égalités
- Distribution des cartes aléatoires
- Gestion du placement des cartes
- Gestion des rounds
- Gestion Gagnant par rounds et par game (un ou plusieurs gagnants par round, et le gagnant de la game est celui qui a gagné le plus de rounds)
- Gestion des scores
- Réutilisation des joueurs, ce qui permet a un joueur de jouer plusieurs games et d'avoir un score global
- Création de joueurs
- Possibilité de changer plusieurs parameters depuis un fichier de configuration (nombre de joueurs, nombre de cartes, nombre de carte à aligner pour gagner, ...)


#### Base de données

- MySql: supporté
- SqLite3: supporté
- MongoDb: supporté

- Connection aux bases de données
- Création des tables
- Insertion des données
- Requêtes de récupération des données
- Requêtes de modification des données
- Requêtes de suppression des données
- Gestion des sauvegardes
- Chargement d'une sauvegarde
- Vidage des tables d'une base
- Changement de base de données en cours d'utilisation (hot swap)
- Extraction des données sous forme csv

### Fonctionnalitées non implementées

#### Jeu
- Jeu à plus de 2 joueurs : non implémenté (Structure implémentée mais la configuration n'est pas finie. Manque pas grand chose mais pas assez de temps)
- Jeu par équipe

#### Bdd
- Génération de données aléatoires : non implémenté (manque de temps)

### Conclusion

- Temps passé sur le projet : > 150h (4-5 h par jours après les cours + ~ 20h - 25h le weekend   
  = 40h minimum x 4 semaines  
  = 160h minimum)

J'ai apprécié ce projet. Cependant, je n'ai pas eu le temps de finir les différentes parties comme je l'aurais voulu.
Je trouve que ce projet m'a pris beaucoup trop de temps personnel par rapport a ce qui est préconisé pour la matière.
De plus, la matière est censé se focaliser sur les basses de données.
Or, 90% du temps de développement a été consacré au développement du jeu, de l'api et du frontend.
Je trouve très frustrant de devoir backer certaines parties et de devoir faire des choix par manque de temps.
Ce projet aurait plus sa place dans une SAE ou un projet de fin d'année qu'en tp sur 3-4 semaines. 

### Annexes
Les diagramMes sont disponibles dans backend/doc pour une meilleure qualité.

1. Diagramme uml des classes du backend. Représentation de l'utilisation du Singleton de GameService
   ![](./backend/doc/GameServics_with_models.png)
2. Diagramme uml des classes du backend. Représentation de l'utilisation de la classe DatabaseFactory
   ![](./backend/doc/Controller_with_service_and_model.svg)
3. Schema de la base de données (aucune différence de conception entre MySql et SqLite3)
  ![](./backend/doc/Entity%20Relationship%20Diagram2.svg)
4. Page de création de partie
  ![](./backend/doc/creation_page.png)
5. Page de jeu
  ![](./backend/doc/exempleJeu1.png)
  ![](./backend/doc/jeuComplet.png)
6. Page d'administration des bases de données
  ![](./backend/doc/adminPage.png)