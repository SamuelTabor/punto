CREATE DATABASE if not exists PUNTO;
USE PUNTO;

CREATE TABLE if not exists Players
(
    id           BIGINT       NOT NULL PRIMARY KEY,
    nom          VARCHAR(255) NOT NULL,
    gameScore    INT          NOT NULL,
    roundScore   INT          NOT NULL,
    created      TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    last_updated TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
);

CREATE TABLE if not exists Games
(
    id             BIGINT                                  NOT NULL PRIMARY KEY,
    description    VARCHAR(255)                            NOT NULL,
    nbRounds       INT                                     NOT NULL,
    gameStatus     ENUM ('STARTED', 'FINISHED', 'WAITING') NOT NULL,
    currentRound   INT                                     NOT NULL,
    flagModifiable BOOLEAN                                 NOT NULL,
    created        TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    last_updated   TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
);

CREATE TABLE if not exists Rounds
(
    id              INT                                     NOT NULL,
    gameId          BIGINT                                  NOT NULL,
    roundStatus     ENUM ('STARTED', 'FINISHED', 'WAITING') NOT NULL,
    currentRoundNum INT                                     NOT NULL,
    flagModifiable  BOOLEAN                                 NOT NULL,
    created         TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    last_updated    TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,

    CONSTRAINT FK_gameId_rounds FOREIGN KEY (gameId) REFERENCES Games (id),
    PRIMARY KEY (id, gameId)
);

CREATE TABLE if not exists Moves
(
    id           INT AUTO_INCREMENT PRIMARY KEY,
    gameId       BIGINT                                  NOT NULL,
    roundId      INT                                     NOT NULL,
    playerId     BIGINT                                  NOT NULL,
    rowIndex     INT                                     NOT NULL,
    colIndex     INT                                     NOT NULL,
    cardNumber   INT                                     NOT NULL,
    cardColor    ENUM ('RED', 'BLUE', 'GREEN', 'YELLOW') NOT NULL,
    allowed      BOOLEAN                                 NOT NULL,
    created      TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    last_updated TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    CONSTRAINT FK_gameId_moves FOREIGN KEY (gameId) REFERENCES Games (id),
    CONSTRAINT FK_roundId_moves FOREIGN KEY (roundId) REFERENCES Rounds (id),
    CONSTRAINT FK_playerId_moves FOREIGN KEY (playerId) REFERENCES Players (id)
);

CREATE TABLE if not exists GameWinners
(
    gameId   BIGINT             NOT NULL,
    playerId BIGINT             NOT NULL,

    CONSTRAINT FK_gameId_gameWinners FOREIGN KEY (gameId) REFERENCES Games (id),
    CONSTRAINT FK_playerId_gameWinners FOREIGN KEY (playerId) REFERENCES Players (id),

    PRIMARY KEY (gameId, playerId)
);

CREATE TABLE if not exists RoundWinners
(
    gameId   BIGINT             NOT NULL,
    roundId  INT                NOT NULL,
    playerId BIGINT             NOT NULL,

    CONSTRAINT FK_gameId_roundWinners FOREIGN KEY (gameId) REFERENCES Games (id),
    CONSTRAINT FK_roundId_roundWinners FOREIGN KEY (roundId) REFERENCES Rounds (id),
    CONSTRAINT FK_playerId_roundWinners FOREIGN KEY (playerId) REFERENCES Players (id),

    PRIMARY KEY (gameId, roundId, playerId)
);

CREATE TABLE if not exists PlayersGames
(
    gameId   BIGINT             NOT NULL,
    playerId BIGINT             NOT NULL,

    CONSTRAINT FK_gameId_playersGames FOREIGN KEY (gameId) REFERENCES Games (id),
    CONSTRAINT FK_playerId_playersGames FOREIGN KEY (playerId) REFERENCES Players (id),

    Primary Key (gameId, playerId)
);

CREATE TABLE if not exists Boards
(
    gameId       BIGINT                                  NOT NULL,
    roundId      INT                                     NOT NULL,
    playerId     BIGINT                                  NOT NULL,
    rowIndex     INT                                     NOT NULL,
    colIndex     INT                                     NOT NULL,
    cardNumber   INT                                     NOT NULL,
    cardColor    ENUM ('RED', 'BLUE', 'GREEN', 'YELLOW') NOT NULL,
    created      TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    last_updated TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,

    CONSTRAINT FK_gameId_boards FOREIGN KEY (gameId) REFERENCES Games (id),
    CONSTRAINT FK_roundId_boards FOREIGN KEY (roundId) REFERENCES Rounds (id),
    CONSTRAINT FK_playerId_boards FOREIGN KEY (playerId) REFERENCES Players (id),

    PRIMARY KEY (gameId, roundId, playerId, rowIndex, colIndex)
);