// Init script for MongoDB

// Create or use the database
db = db.getSiblingDB('PUNTO');

//Create collection Users
db.createCollection('Players', {
    validator: {
        $jsonSchema: {
            bsonType: 'object',
            required: ['playerId', 'nom', 'gameScore', 'roundScore', 'created', 'last_updated'],
            properties: {
                playerId: {
                    bsonType: 'long',
                    description: 'must be a long and is required'
                },
                nom: {
                    bsonType: 'string',
                    description: 'must be a string and is required'
                },
                gameScore: {
                    bsonType: 'int',
                    description: 'must be a int and is required'
                },
                roundScore: {
                    bsonType: 'int',
                    description: 'must be a int and is required'
                },
                created: {
                    bsonType: 'date',
                    description: 'must be a date and is required'
                },
                last_updated: {
                    bsonType: 'date',
                    description: 'must be a date and is required'
                }
            }
        }
    }
});

//Create collection Games
db.createCollection('Games', {

    validator: {
        $jsonSchema: {
            bsonType: 'object',
            required: ['gameId', 'nbRounds', 'players', 'rounds', 'gameStatus', 'winners', 'currentRound', 'description', 'flagModifiable', 'created', 'last_updated'],
            properties: {
                gameId: {
                    bsonType: 'long',
                    description: 'must be a long and is required'
                },
                nbRounds: {
                    bsonType: 'int',
                    description: 'must be a int and is required'
                },
                players: {
                    bsonType: 'array',
                    items: {
                        bsonType: 'long',
                        minItems: 2,
                        maxItems: 8,
                        description: 'must be a array of long and is required (at least 2)'
                    },
                    description: 'must be a array and is required'
                },
                rounds: {
                    bsonType: 'array',
                    items: {
                        bsonType: 'long',
                        minItems: 1,
                        description: 'must be a array of long and is required (at least 1)'
                    },
                    description: 'must be a array and is required'
                },
                gameStatus: {
                    enum: ['CREATED', 'STARTED', 'ENDED'],
                    description: 'can only be one of the enum values and is required'
                },
                winners: {
                    bsonType: 'array',
                    items: {
                        bsonType: 'long'
                    },
                    description: 'must be a array and is required'
                },
                currentRound: {
                    bsonType: 'int',
                    description: 'must be a int and is required'
                },
                description: {
                    bsonType: 'string',
                    description: 'must be a string and is required'
                },
                flagModifiable: {
                    bsonType: 'bool',
                    description: 'must be a bool and is required'
                },
                created: {
                    bsonType: 'date',
                    description: 'must be a date and is required'
                },
                last_updated: {
                    bsonType: 'date',
                    description: 'must be a date and is required'
                }
            }
        }
    }
});

//Create collection Rounds
db.createCollection('Rounds', {
    validator: {
        $jsonSchema: {
            bsonType: 'object',
            required: ['roundId', 'gameId', 'winners', 'roundStatus', 'numRound', 'flagModifiable', 'created', 'last_updated'],
            properties: {
                roundId: {
                    bsonType: 'long',
                    description: 'must be a long and is required'
                },
                gameId: {
                    bsonType: 'long',
                    description: 'must be a long and is required'
                },
                winners: {
                    bsonType: 'array',
                    items: {
                        bsonType: 'long'
                    },
                    description: 'must be a array and is required'
                },
                roundStatus: {
                    enum: ['CREATED', 'STARTED', 'ENDED'],
                    description: 'can only be one of the enum values and is required'
                },
                numRound: {
                    bsonType: 'int',
                    description: 'must be a int and is required'
                },
                flagModifiable: {
                    bsonType: 'bool',
                    description: 'must be a bool and is required'
                },
                created: {
                    bsonType: 'date',
                    description: 'must be a date and is required'
                },
                last_updated: {
                    bsonType: 'date',
                    description: 'must be a date and is required'
                }
            }
        }
    }
});

//Create collection Moves
db.createCollection('Moves', {
validator: {
    $jsonSchema: {
        bsonType: 'object',
        required: ['gameId', 'roundId', 'playerId', 'rowIndex', 'colIndex', 'cardColor', 'cardNumber', 'isAllowed', 'created', 'last_updated'],
        properties: {
            gameId: {
                bsonType: 'long',
                description: 'must be a long and is required'
            },
            roundId: {
                bsonType: 'int',
                description: 'must be a int and is required'
            },
            playerId: {
                bsonType: 'long',
                description: 'must be a long and is required'
            },
            rowIndex: {
                bsonType: 'int',
                description: 'must be a int and is required'
            },
            colIndex: {
                bsonType: 'int',
                description: 'must be a int and is required'
            },
            cardColor: {
                enum: ['RED', 'BLUE', 'GREEN', 'YELLOW', 'PURPLE', 'WHITE'],
                description: 'can only be one of the enum values and is required'
            },
            cardNumber: {
                bsonType: 'int',
                description: 'must be a int and is required'
            },
            isAllowed: {
                bsonType: 'bool',
                description: 'must be a bool and is required'
            },
            created: {
                bsonType: 'date',
                description: 'must be a date and is required'
            },
            last_updated: {
                bsonType: 'date',
                description: 'must be a date and is required'
            }
        }
    }
}
});