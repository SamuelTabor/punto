CREATE TABLE IF NOT EXISTS Players
(
    id           INTEGER PRIMARY KEY NOT NULL,
    nom          TEXT                NOT NULL,
    gameScore    INTEGER             NOT NULL,
    roundScore   INTEGER             NOT NULL,
    created      TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    last_updated TIMESTAMP DEFAULT CURRENT_TIMESTAMP
);

CREATE TABLE IF NOT EXISTS Games
(
    id             INTEGER PRIMARY KEY                                           NOT NULL,
    description    TEXT                                                          NOT NULL,
    nbRounds       INTEGER                                                       NOT NULL,
    gameStatus     TEXT CHECK (gameStatus IN ('STARTED', 'FINISHED', 'WAITING')) NOT NULL,
    currentRound   INTEGER                                                       NOT NULL,
    flagModifiable BOOLEAN                                                       NOT NULL,
    created        TIMESTAMP DEFAULT CURRENT_TIMESTAMP                           NOT NULL,
    last_updated   TIMESTAMP DEFAULT CURRENT_TIMESTAMP
);

CREATE TABLE IF NOT EXISTS Rounds
(
    id              INTEGER                                                        NOT NULL,
    gameId          INTEGER                                                        NOT NULL,
    roundStatus     TEXT CHECK (roundStatus IN ('STARTED', 'FINISHED', 'WAITING')) NOT NULL,
    currentRoundNum INTEGER                                                        NOT NULL,
    flagModifiable  BOOLEAN                                                        NOT NULL,
    created         TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    last_updated    TIMESTAMP DEFAULT CURRENT_TIMESTAMP,

    CONSTRAINT FK_gameId_rounds FOREIGN KEY (gameId) REFERENCES Games (id),
    PRIMARY KEY (id, gameId)
);

CREATE TABLE IF NOT EXISTS Moves
(
    id           INTEGER PRIMARY KEY AUTOINCREMENT,
    gameId       INTEGER                                                      NOT NULL,
    roundId      INTEGER                                                      NOT NULL,
    playerId     INTEGER                                                      NOT NULL,
    rowIndex     INTEGER                                                      NOT NULL,
    colIndex     INTEGER                                                      NOT NULL,
    cardNumber   INTEGER                                                      NOT NULL,
    cardColor    TEXT CHECK (cardColor IN ('RED', 'BLUE', 'GREEN', 'YELLOW')) NOT NULL,
    allowed      BOOLEAN                                                      NOT NULL,
    created      TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    last_updated TIMESTAMP DEFAULT CURRENT_TIMESTAMP,

    CONSTRAINT FK_gameId_moves FOREIGN KEY (gameId) REFERENCES Games (id),
    CONSTRAINT FK_roundId_moves FOREIGN KEY (roundId) REFERENCES Rounds (id),
    CONSTRAINT FK_playerId_moves FOREIGN KEY (playerId) REFERENCES Players (id)
);

CREATE TABLE IF NOT EXISTS GameWinners
(
    gameId   INTEGER NOT NULL,
    playerId INTEGER NOT NULL,

    CONSTRAINT FK_gameId_gameWinners FOREIGN KEY (gameId) REFERENCES Games (id),
    CONSTRAINT FK_playerId_gameWinners FOREIGN KEY (playerId) REFERENCES Players (id),

    PRIMARY KEY (gameId, playerId)
);

CREATE TABLE IF NOT EXISTS RoundWinners
(
    gameId   INTEGER NOT NULL,
    roundId  INTEGER NOT NULL,
    playerId INTEGER NOT NULL,

    CONSTRAINT FK_gameId_roundWinners FOREIGN KEY (gameId) REFERENCES Games (id),
    CONSTRAINT FK_roundId_roundWinners FOREIGN KEY (roundId) REFERENCES Rounds (id),
    CONSTRAINT FK_playerId_roundWinners FOREIGN KEY (playerId) REFERENCES Players (id),

    PRIMARY KEY (gameId, roundId, playerId)
);

CREATE TABLE IF NOT EXISTS PlayersGames
(
    gameId   INTEGER                           NOT NULL,
    playerId INTEGER                           NOT NULL,

    CONSTRAINT FK_gameId_playersGames FOREIGN KEY (gameId) REFERENCES Games (id),
    CONSTRAINT FK_playerId_playersGames FOREIGN KEY (playerId) REFERENCES Players (id),

    PRIMARY KEY (gameId, playerId)
);

CREATE TABLE IF NOT EXISTS Boards
(
    gameId       INTEGER                                                      NOT NULL,
    roundId      INTEGER                                                      NOT NULL,
    playerId     INTEGER                                                      NOT NULL,
    rowIndex     INTEGER                                                      NOT NULL,
    colIndex     INTEGER                                                      NOT NULL,
    cardNumber   INTEGER                                                      NOT NULL,
    cardColor    TEXT CHECK (cardColor IN ('RED', 'BLUE', 'GREEN', 'YELLOW')) NOT NULL,
    created      TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    last_updated TIMESTAMP DEFAULT CURRENT_TIMESTAMP,

    CONSTRAINT FK_gameId_boards FOREIGN KEY (gameId) REFERENCES Games (id),
    CONSTRAINT FK_roundId_boards FOREIGN KEY (roundId) REFERENCES Rounds (id),
    CONSTRAINT FK_playerId_boards FOREIGN KEY (playerId) REFERENCES Players (id),

    PRIMARY KEY (gameId, roundId, playerId, rowIndex, colIndex)
);