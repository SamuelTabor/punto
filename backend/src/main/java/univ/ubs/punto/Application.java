package univ.ubs.punto;

import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.info.Contact;
import io.swagger.v3.oas.annotations.info.Info;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import univ.ubs.punto.model.Game;
import univ.ubs.punto.model.Player;
import univ.ubs.punto.service.GameService;

import java.util.ArrayList;

@SpringBootApplication
@OpenAPIDefinition(
		info = @Info(
				title = "Punto API",
				version = "1.0",
				description = "Punto API documentation",
				contact = @Contact(
						name = "TABOR Samuel",
						email = "tabor.e2102105@etud.univ-ubs.fr"
				)
		)
)
public class Application {

	public static void main(String[] args) {
		SpringApplication.run(Application.class, args);
	}

	/*@Bean
	public CommandLineRunner initData() {
		return (args) -> {
			ArrayList<Player> players = new ArrayList<>();
			players.add(new Player("Samuel", null));
			players.add(new Player("Alexandre", null));
			players.add(new Player("Mickael", null));

			Game game = new Game(players, 3, "Game 1", false);
			Game game2 = new Game(players, 3, "Game 2", false);

			for (Player player : players) {
				GameService.getInstance().addPlayer(player);
			}

			GameService.getInstance().addGame(game);
			GameService.getInstance().addGame(game2);
		};
	}*/

}
