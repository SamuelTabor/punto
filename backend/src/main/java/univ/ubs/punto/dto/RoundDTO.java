package univ.ubs.punto.dto;

import org.json.JSONArray;
import org.json.JSONObject;
import univ.ubs.punto.exceptions.RoundException;
import univ.ubs.punto.model.*;
import univ.ubs.punto.service.GameService;

import java.util.ArrayList;

public class RoundDTO {
    private long gameID;
    private int roundId;
    private int roundNumber;
    private GameStatus status;
    private ArrayList<Player> winners;
    private Player currentPlayer;
    private Card currentCard;
    private BoardDTO board;


    public RoundDTO(long gameID, int roundId, int roundNumber, GameStatus status, ArrayList<Player> winners, Player currentPlayer, Card currentCard) {
        this.gameID = gameID;
        this.roundId = roundId;
        this.roundNumber = roundNumber;
        this.status = status;
        this.winners = winners;
        this.currentPlayer = currentPlayer;
        this.currentCard = currentCard;
    }

    public RoundDTO(Round round) throws RoundException {
        Game game = GameService.getInstance().getGameByID(round.getGameId());
        JSONArray winners = new JSONArray();

        if (round.getWinners() != null) {
            for (Player winner : round.getWinners()) {
                winners.put(winner.toJson());
            }
        }

        this.gameID = round.getGameId();
        this.roundId = round.getRoundId();
        this.roundNumber = round.getNumRound();
        this.status = round.getRoundStatus();
        this.winners = round.getWinners();
        this.currentPlayer = (round.getRoundStatus() == GameStatus.FINISHED) ? null : game.getPlayers().get(round.getNumRound() % game.getPlayers().size());
        this.currentCard = (round.getRoundStatus() == GameStatus.FINISHED) ? null : round.getFirstCard(this.currentPlayer);
        this.board = new BoardDTO(round.getBoard());
    }

    public long getGameID() {
        return gameID;
    }

    public void setGameID(long gameID) {
        this.gameID = gameID;
    }

    public int getRoundId() {
        return roundId;
    }

    public void setRoundId(int roundId) {
        this.roundId = roundId;
    }

    public int getRoundNumber() {
        return roundNumber;
    }

    public void setRoundNumber(int roundNumber) {
        this.roundNumber = roundNumber;
    }

    public GameStatus getStatus() {
        return status;
    }

    public void setStatus(GameStatus status) {
        this.status = status;
    }

    public ArrayList<Player> getWinners() {
        return winners;
    }

    public void setWinner(ArrayList<Player> winners) {
        this.winners = winners;
    }

    public Player getCurrentPlayer() {
        return currentPlayer;
    }

    public void setCurrentPlayer(Player currentPlayer) {
        this.currentPlayer = currentPlayer;
    }

    public Card getCurrentCard() {
        return currentCard;
    }

    public void setCurrentCard(Card currentCard) {
        this.currentCard = currentCard;
    }

    public JSONObject toJson() {
        JSONObject json = new JSONObject();
        JSONArray winnersJson = new JSONArray();

        if (winners != null) {
            for (Player winner : winners) {
                winnersJson.put(winner.toJson());
            }
        }

        json.put("gameID", gameID);
        json.put("roundId", roundId);
        json.put("roundNumber", roundNumber);
        json.put("status", status);
        json.put("winners", winnersJson);
        json.put("currentPlayer", (currentPlayer == null) ? null : currentPlayer.toJson());
        json.put("currentCard", (currentCard == null) ? null : currentCard.toJson());
        json.put("board", board.toJson());

        return json;
    }
}
