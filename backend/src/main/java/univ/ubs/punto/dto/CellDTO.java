package univ.ubs.punto.dto;

import org.json.JSONObject;
import univ.ubs.punto.model.Card;

public class CellDTO {
    private int row;
    private int column;
    private boolean isPlayable;
    private Card card;

    public CellDTO(int row, int column, boolean isPlayable, Card card) {
        this.row = row;
        this.column = column;
        this.isPlayable = isPlayable;
        this.card = card;
    }

    public int getRow() {
        return row;
    }

    public void setRow(int row) {
        this.row = row;
    }

    public int getColumn() {
        return column;
    }

    public void setColumn(int column) {
        this.column = column;
    }

    public boolean isPlayable() {
        return isPlayable;
    }

    public void setPlayable(boolean playable) {
        isPlayable = playable;
    }

    public Card getCard() {
        return card;
    }

    public void setCard(Card card) {
        this.card = card;
    }

    public JSONObject toJson() {
        JSONObject json = new JSONObject();
        json.put("row", this.row);
        json.put("column", this.column);
        json.put("isPlayable", this.isPlayable);
        json.put("card", this.card != null ? this.card.toJson() : null);
        return json;
    }
}
