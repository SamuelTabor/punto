package univ.ubs.punto.dto;

import org.json.JSONArray;
import org.json.JSONObject;
import univ.ubs.punto.model.Board;

import java.awt.*;
import java.util.ArrayList;
import java.util.Set;

public class BoardDTO {
    private ArrayList<ArrayList<CellDTO>> boardDTO = new ArrayList<>();

    public BoardDTO(Board board) {
        // convert cells to cellDTOs
        Set<Point> playableCells = board.getPlayableCells();

        for (int row = 0; row < board.getBoard().size(); row++) {
            ArrayList<CellDTO> rowDTO = new ArrayList<>();
            for (int col = 0; col < board.getBoard().get(row).size(); col++) {
                Point cell = new Point(row, col);
                boolean isPlayable = playableCells.contains(cell);
                CellDTO cellDTO = new CellDTO(row, col, isPlayable, board.getBoard().get(row).get(col).getCard());
                rowDTO.add(cellDTO);
            }
            boardDTO.add(rowDTO);
        }
    }

    public ArrayList<ArrayList<CellDTO>> getBoardDTO() {
        return boardDTO;
    }

    public void setBoardDTO(ArrayList<ArrayList<CellDTO>> boardDTO) {
        this.boardDTO = boardDTO;
    }

    public JSONArray toJson() {
        JSONArray boardJson = new JSONArray();
        for (ArrayList<CellDTO> row : boardDTO) {
            JSONArray rowJson = new JSONArray();
            for (CellDTO cell : row) {
                rowJson.put(cell.toJson());
            }
            boardJson.put(rowJson);
        }

        return boardJson;
    }
}
