package univ.ubs.punto.dto;

import org.json.JSONArray;
import org.json.JSONObject;
import univ.ubs.punto.Config;
import univ.ubs.punto.exceptions.RoundException;
import univ.ubs.punto.model.Game;
import univ.ubs.punto.model.GameStatus;
import univ.ubs.punto.model.Player;
import univ.ubs.punto.model.Round;
import univ.ubs.punto.service.GameService;

import java.util.ArrayList;



public class GameDTO {
    private long gameId;

    private int nbRounds;

    private ArrayList<Player> players;
    private String description;
    private GameStatus status;
    private int currentRound;
    private ArrayList<Player> winners;

    public GameDTO(ArrayList<Player> players, int nbRounds, String description) {
        this.nbRounds = nbRounds;
        this.players = players;
        this.description = description;
    }

    public GameDTO(Game game) {
        if (game == null) throw new IllegalArgumentException("game must not be null");
        this.gameId = game.getGameId();
        this.nbRounds = game.getNbRounds();
        this.players = game.getPlayers();
        this.description = game.getGameDescription();
        this.status = game.getGameStatus();
        this.currentRound = game.getCurrentRound().getRoundId();
        this.winners = game.getGameWinners();
    }

    public long getGameId() {
        return gameId;
    }

    public void setGameId(long gameId) {
        this.gameId = gameId;
    }

    public int getNbRounds() {
        return nbRounds;
    }

    public void setNbRounds(int nbRounds) {
        this.nbRounds = nbRounds;
    }

    public ArrayList<Player> getPlayers() {
        return players;
    }

    public void setPlayers(ArrayList<Player> players) {
        this.players = players;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public GameStatus getStatus() {
        return status;
    }

    public void setStatus(GameStatus status) {
        this.status = status;
    }

    public int getCurrentRound() {
        return currentRound;
    }

    public void setCurrentRound(int currentRound) {
        this.currentRound = currentRound;
    }

    public ArrayList<Player> getWinners() {
        return winners;
    }

    public void setWinner(ArrayList<Player> winners) {
        this.winners = winners;
    }

    public Game toGame() {
        return new Game(players, nbRounds, description, false);
    }

    public JSONObject toJson() throws RoundException {
        JSONArray playersJson = new JSONArray();
        JSONArray allRoundsJson = new JSONArray();
        JSONArray winnersJson = new JSONArray();

        for (Player player : players) {
            playersJson.put(player.toJson());
        }

        for (Round round : GameService.getInstance().getGameByID(gameId).getRounds()) {
            allRoundsJson.put(new RoundDTO(round).toJson());
        }

        for (Player winner : winners) {
            winnersJson.put(winner.toJson());
        }
        JSONObject gameJson = new JSONObject();
        gameJson.put("gameId", gameId);
        gameJson.put("nbRounds", nbRounds);
        gameJson.put("players", playersJson);
        gameJson.put("description", description);
        gameJson.put("status", status);
        gameJson.put("winners", winnersJson);
        gameJson.put("currentRound", currentRound);
        gameJson.put("allRounds", allRoundsJson);
        gameJson.put("database", Config.DATABASE.toString());
        return gameJson;
    }
}
