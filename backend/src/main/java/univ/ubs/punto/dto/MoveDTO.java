package univ.ubs.punto.dto;

import org.json.JSONObject;
import univ.ubs.punto.model.Card;
import univ.ubs.punto.model.Color;

public class MoveDTO {
    private long playerId;
    private int roundId;
    private int row;
    private int column;
    private String cardColor;
    private int cardNumber;

    public MoveDTO(long playerId, int round, int row, int column, String cardColor, int cardNumber) {
        this.playerId = playerId;
        this.roundId = round;
        this.row = row;
        this.column = column;
        this.cardColor = cardColor;
        this.cardNumber = cardNumber;
    }

    public long getPlayerId() {
        return playerId;
    }

    public void setPlayerId(long playerId) {
        this.playerId = playerId;
    }

    public int getRoundId() {
        return roundId;
    }

    public void setRoundId(int roundId) {
        this.roundId = roundId;
    }

    public int getRow() {
        return row;
    }

    public void setRow(int row) {
        this.row = row;
    }

    public int getColumn() {
        return column;
    }

    public void setColumn(int column) {
        this.column = column;
    }

    public String getCardColor() {
        return cardColor;
    }

    public void setCardColor(String cardColor) {
        this.cardColor = cardColor;
    }

    public int getCardNumber() {
        return cardNumber;
    }

    public void setCardNumber(int cardNumber) {
        this.cardNumber = cardNumber;
    }

    public JSONObject toJson() {
        JSONObject json = new JSONObject();
        json.put("playerId", playerId);
        json.put("roundId", roundId);
        json.put("row", row);
        json.put("column", column);
        json.put("card", new Card(cardNumber, Color.valueOf(cardColor)).toJson());
        return json;
    }
}
