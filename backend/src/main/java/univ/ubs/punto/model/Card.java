package univ.ubs.punto.model;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import univ.ubs.punto.Config;

import java.util.ArrayList;

public class Card implements Comparable<Card> {
    private static final Logger logger = LoggerFactory.getLogger(Card.class);
    private int cardNumber;
    private Color cardColor;

    //****************************************************************************************************************//
    //************************************************** CONSTRUCTORS ***********************************************//

    public Card() {
        this.cardNumber = -1;
        this.cardColor = null;
    }

    public Card(int number, Color color) {
        if (color == null) {
            throw new IllegalArgumentException("Color must not be null");
        }
        if (number < 1 || number > Config.NUMBER_OF_CARDS) {
            throw new IllegalArgumentException(String.format("Card number must be between 1 and %d included : %d", Config.NUMBER_OF_CARDS, number));
        }
        this.cardNumber = number;
        this.cardColor = color;
        logger.debug(String.format("Card created : %d %s", number, color));
    }

    //****************************************************************************************************************//
    //************************************************** GETTERS ****************************************************//

    public int getCardNumber() {
        return this.cardNumber;
    }

    public Color getCardColor() {
        return this.cardColor;
    }

    //****************************************************************************************************************//
    //************************************************** SETTERS ****************************************************//

    public void setCardNumber(int cardNumber) {
        this.cardNumber = cardNumber;
        logger.debug(String.format("Card number set to %d", cardNumber));
    }

    public void setCardColor(Color cardColor) {
        if (this.cardColor == null) {
            throw new IllegalArgumentException("Color must not be null");
        }
        this.cardColor = cardColor;
        logger.debug(String.format("Card color set to %s", cardColor));
    }

    //****************************************************************************************************************//
    //************************************************** METHODS ****************************************************//

    /**
     * Create a set of {@value Config#NUMBER_OF_CARDS} cards for a given color
     * @param color the color of the cards
     * @return an array list of cards
     */
    public static ArrayList<Card> createSetOfCards(Color color) {
        ArrayList<Card> res = new ArrayList<>();
        for (int i = 1; i <= Config.NUMBER_OF_CARDS; i++) {
            res.add(new Card(i, color));
        }
        logger.debug(String.format("Set of cards created for color %s", color));
        return res;
    }
    @Override
    public int compareTo(Card o) {
        if (o == null) {
            throw new IllegalArgumentException("Card must not be null");
        } else {
            if (this.cardNumber < o.cardNumber) {
                return -1;
            } else if (this.cardNumber > o.cardNumber) {
                return 1;
            } else {
                return 0;
            }
        }
    }

    @Override
    public boolean equals(Object o) {
        if (o != null) {
            if (o instanceof Card c) {
                if (this.cardNumber == c.cardNumber) {
                    if (this.cardColor == null && c.cardColor == null) {
                        return true;
                    }
                    if (this.cardColor != null) {
                        return this.cardColor.equals(c.cardColor);
                    }
                }
            }
        }
        return false;
    }

    //****************************************************************************************************************//
    //********************************************* toString() & toJson() ********************************************//
    public String toString() {
        return String.format("%d %s", this.cardNumber, this.cardColor);
    }

    public JSONObject toJson() {
        return new JSONObject()
                .put("cardNumber", this.cardNumber)
                .put("cardColor", this.cardColor);
    }
}
