package univ.ubs.punto.model;

import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import univ.ubs.punto.Config;
import univ.ubs.punto.database.DatabaseFactory;
import univ.ubs.punto.database.mysql.MySqlException;
import univ.ubs.punto.exceptions.GameException;
import univ.ubs.punto.exceptions.RoundException;
import univ.ubs.punto.service.GameService;

import java.util.*;

public class Round {
    /**
     * Represents the logger of the class
     */
    private static final Logger logger = LoggerFactory.getLogger(Round.class);
    private static final GameService gameService = GameService.getInstance();

    /**
     * Represents the id of the round
     */
    private int roundId;

    /**
     * Represents the game of the round
     */
    private long gameId;

    /**
     * Represents the winner of the round
     */
    private ArrayList<Player> winners;

    /**
     * Represents the status of the round: WAITING, STARTED or FINISHED
     */
    private GameStatus roundStatus;

    /**
     * Represents the board of the round
     */
    private Board board;

    /**
     * Represents the map of players and their cards
     */
    private final Map<Player, ArrayList<Card>> playerCardsMap;

    /**
     * Represents the index of the current round
     */
    private int numRound = 0;

    /**
     * Flag to define if the game is modifiable without restrictions any by the app
     **/
    private final boolean flagModifiable;

    //****************************************************************************************************************//
    //************************************************** CONSTRUCTORS ***********************************************//

    /**
     * Constructor of the class Round. It initializes the board when the round is created
     *
     * @param game    the game of the round
     * @param roundId the id of the round
     */
    public Round(Game game, int roundId, boolean flagModifiable) {
        if (game == null) {
            throw new IllegalArgumentException("Game cannot be null");
        }
        if (roundId < 0) {
            throw new IllegalArgumentException("Round id cannot be negative");
        }
        if (roundId >= game.getNbRounds()) {
            throw new IllegalArgumentException(String.format("Round id cannot be greater than %d for the game %d", game.getNbRounds() - 1, game.getGameId()));
        }
        if (game.getPlayers().isEmpty()) {
            throw new IllegalArgumentException("You are trying to create a round for a game with no players");
        }
        this.roundId = roundId;
        this.gameId = game.getGameId();
        this.winners = new ArrayList<>();
        this.roundStatus = GameStatus.WAITING;
        this.board = new Board(this.gameId, this.roundId, false);
        this.playerCardsMap = initPlayersCardsMap();
        this.flagModifiable = flagModifiable;
        logger.debug(String.format("Round %d created for the game %d", this.roundId, this.gameId));
    }

    //****************************************************************************************************************//
    //************************************************** GETTERS ****************************************************//

    public int getRoundId() {
        return roundId;
    }

    public long getGameId() {
        return gameId;
    }

    public ArrayList<Player> getWinners() {
        return this.winners;
    }

    public GameStatus getRoundStatus() {
        return roundStatus;
    }

    public Board getBoard() {
        return board;
    }

    public Map<Player, ArrayList<Card>> getPlayerCardsMap() {
        return playerCardsMap;
    }

    public int getNumRound() {
        return numRound;
    }

    public boolean getFlagModifiable() {
        return this.flagModifiable;
    }

    //****************************************************************************************************************//
    //************************************************** SETTERS ****************************************************//

    public void setRoundId(Integer roundId) throws RoundException {
        if (!flagModifiable) throw new RoundException("Cannot modify the round id if the round is not modifiable");
        if (roundId == null) throw new IllegalArgumentException("roundId must not be null");
        if (roundId < 0) throw new IllegalArgumentException("roundId must not be negative: %d".formatted(roundId));

        this.roundId = roundId;
        logger.debug(String.format("Round id updated to %d", this.roundId));
    }

    public void setGameId(Integer gameId) throws RoundException {
        if (!flagModifiable) throw new RoundException("Cannot modify the game id if the round is not modifiable");
        if (gameId == null) throw new IllegalArgumentException("gameId must not be null");
        if (gameId < 0) throw new IllegalArgumentException("gameId must not be negative: %d".formatted(gameId));

        this.gameId = gameId;
        logger.debug(String.format("Round id updated to %d", this.gameId));
    }

    public void setBoard(Board board) throws RoundException {
        if (!this.flagModifiable) throw new RoundException("Cannot modify the board if the game is not modifiable");
        if (board == null) throw new IllegalArgumentException("board must not be null");

        this.board = board;
        logger.debug(String.format("Round %d board updated", this.roundId));
    }

    /**
     * Set the winner of the round
     *
     * @param winners the winner of the round
     * @throws IllegalArgumentException if the winner is null
     * @throws RoundException           if the winner is not in the game, or if the round status is WAITING or FINISHED
     */
    public void setWinners(ArrayList<Player> winners) throws IllegalArgumentException, RoundException {
        if (winners == null || winners.isEmpty()) throw new IllegalArgumentException("Round winner cannot be null");
        if (!this.flagModifiable) {
            //For each winners, check if they are in the game
            for (Player winner : winners) {
                if (!gameService.getGameByID(this.gameId).getPlayers().contains(winner))
                    throw new RoundException(String.format("Round winner %d is not in the game %d", winner.getPlayerId(), this.gameId));
            }

            //game status is WAITING or FINISHED
            if (this.roundStatus != GameStatus.STARTED)
                throw new RoundException(String.format("Cannot set a round winner if the round is %s", this.roundStatus));
        }
        logger.debug(String.format("Round %d winner set to %s", this.roundId, winners));
        this.winners = winners;
    }

    /**
     * Set the status of the round. It can be WAITING, STARTED or FINISHED
     *
     * @param status the status of the round
     * @throws IllegalArgumentException if the status is null
     * @throws RoundException           if the status is WAITING and the round has no rounds, or if the status is STARTED and the round has no winner
     */
    public void setRoundStatus(GameStatus status) throws IllegalArgumentException, RoundException {
        if (status == null) throw new IllegalArgumentException("Round status cannot be null");

        if (!flagModifiable) {
            if (this.roundStatus == status) logger.warn(String.format("Round status is already %s", status));

            //WAITING --> FINISHED: KO
            if (this.roundStatus == GameStatus.WAITING && status == GameStatus.FINISHED)
                throw new RoundException("Cannot set a round to FINISHED if it's not STARTED");

            //STARTED --> WAITING: KO
            if (this.roundStatus == GameStatus.STARTED && status == GameStatus.WAITING)
                throw new RoundException("Cannot set a round to WAITING if it's not FINISHED");

            //STARTED --> FINISHED: KO because no winner
            if (this.roundStatus == GameStatus.STARTED && status == GameStatus.FINISHED && this.winners.isEmpty())
                throw new RoundException("Cannot set a round to FINISHED if it has no winner");

            //FINISHED --> WAITING | STARTED: KO
            if (this.roundStatus == GameStatus.FINISHED && ((status == GameStatus.WAITING) || status == GameStatus.STARTED))
                throw new RoundException(String.format("Cannot set a round to %s if it's already FINISHED", status));
        }
        this.roundStatus = status;
        logger.debug(String.format("Round status set to %s", status));
    }

    public boolean play(Player player, Card card, int xPos, int yPos) throws RoundException, GameException {

        if (player == null) throw new IllegalArgumentException("player must not be null");
        if (card == null) throw new IllegalArgumentException("card must not be null");
        if (xPos < 0 || xPos > (Config.BOARD_SIZE * 2 - 1))
            throw new IllegalArgumentException("xPos must be between 1 and %d: %d".formatted((Config.BOARD_SIZE * 2 - 1), xPos));
        if (yPos < 0 || yPos > (Config.BOARD_SIZE * 2 - 1))
            throw new IllegalArgumentException("yPos must be between 1 and %d: %d".formatted((Config.BOARD_SIZE * 2 - 1), yPos));
        if (this.roundStatus != GameStatus.STARTED)
            throw new RoundException(String.format("Cannot play if the round is %s", this.roundStatus));
        if (!this.winners.isEmpty())
            throw new RoundException(String.format("Cannot play if the round has a winner: %s", this.winners));
        if (!this.playerCardsMap.containsKey(player))
            throw new RoundException(String.format("Cannot play if the player %s is not in the round", player));

        //check if it's the right player's turn
        if (this.numRound % gameService.getGameByID(this.gameId).getPlayers().size() != gameService.getGameByID(this.gameId).getPlayers().indexOf(player)) {
            throw new RoundException(String.format("Cannot play if it's not the player %s's turn", player));
        }

        boolean res = false;
        //Check if the player has cards remaining
        if (!this.getPlayerCardsMap().get(player).isEmpty()) {
            //if yes, check if the card is in the player's hand
            if (!this.getPlayerCardsMap().get(player).contains(card)) {
                throw new RoundException(String.format("Cannot play if the player %s does not have the card %s", player, card));
            } else {
                //try to play a move
                if (this.board.updateBoard(card, player, xPos, yPos)) {
                    //if the move is successful, remove the card from the player's cards
                    logger.debug("A move was made successfully");
                    this.getPlayerCardsMap().get(player).remove(card);
                    this.numRound++;
                    res = true;
                } else {
                    //Move not allowed
                    logger.debug("Move not allowed");
                }
            }
        }

        // Check if all players have no cards remaining
        boolean allPlayersOutOfCards = this.playerCardsMap.values().stream()
                .allMatch(ArrayList::isEmpty);

        if (allPlayersOutOfCards && this.winners.isEmpty()) {
            // If all players are out of cards and no one has won, finish the round without a winner
            this.winners.addAll(GameService.getInstance().getGameByID(this.gameId).getPlayers());
            this.setRoundStatus(GameStatus.FINISHED);
            logger.debug("All players are out of cards. The round is finished. Equality, all players won");
        }

        gameService.getGameByID(this.gameId).updateGame();
        try {
            DatabaseFactory.getInstance().insert(this.gameId, this.roundId, player.getPlayerId(), xPos, yPos, card.getCardColor().toString(), card.getCardNumber(), res);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
        return res;
    }

    //****************************************************************************************************************//
    //************************************************** METHODS ****************************************************//

    /**
     * Initialise players' deck depending on the numbers of players
     *
     * @return a map of each player associated with its set of cards
     */
    private Map<Player, ArrayList<Card>> initPlayersCardsMap() {
        Map<Player, ArrayList<Card>> res = new HashMap<>();
        ArrayList<Player> players = gameService.getGameByID(gameId).getPlayers();
        ArrayList<Color> colors = new ArrayList<>(List.of(Color.values()));
        Random random = new Random();

        while (colors.size() >= players.size() && !players.isEmpty()) {
            for (Player player : players) {
                random.nextLong();
                Color randomColor = colors.get(random.nextInt(colors.size()));
                ArrayList<Card> newCards = Card.createSetOfCards(randomColor);
                newCards.addAll(Card.createSetOfCards(randomColor));
                res.compute(player, (k, v) -> {
                    if (v == null) {
                        return newCards; // Si le joueur n'est pas déjà dans la map, on crée une nouvelle liste de cartes
                    } else {
                        v.addAll(newCards); // Si le joueur est déjà dans la map, on ajoute les nouvelles cartes à la liste existante
                        return v;
                    }
                });
                Collections.shuffle(res.get(player), random);
                colors.remove(randomColor);
            }
        }
        return res;
    }

    public Card getFirstCard(Player player) throws RoundException {
        if (player == null) throw new IllegalArgumentException("player must not be null");
        if (!this.playerCardsMap.containsKey(player))
            throw new RoundException(String.format("Cannot get the first card if the player %s is not in the round", player));
        if (this.playerCardsMap.get(player).isEmpty())
            throw new RoundException.EmplyPlayerCardsHandException(String.format("Cannot get the first card if the player %s has no cards", player));

        return this.playerCardsMap.get(player).get(0);
    }

    //****************************************************************************************************************//
    //********************************************* toString() & toJson() ********************************************//

    public JSONObject toJson() {
        JSONObject roundJson = new JSONObject();
        JSONObject playerCardsMapJson = new JSONObject();

        for (Map.Entry<Player, ArrayList<Card>> entry : this.playerCardsMap.entrySet()) {
            JSONArray cardsJsonArray = new JSONArray();
            for (Card card : entry.getValue()) {
                cardsJsonArray.put(new JSONObject(card.toJson()));
            }
            playerCardsMapJson.put(String.valueOf(entry.getKey().getPlayerId()), cardsJsonArray);
        }

        if (!this.winners.isEmpty()) {
            JSONArray winnersJson = new JSONArray();
            for (Player winner : this.winners) {
                winnersJson.put(new JSONObject(winner.toJson()));
            }
            roundJson.put("winners", winnersJson);
        } else {
            roundJson.put("winners", JSONObject.NULL);
        }

        roundJson.put("roundId", this.roundId);
        roundJson.put("gameId", this.gameId);
        roundJson.put("roundStatus", this.roundStatus.toString());
        roundJson.put("board", new JSONObject(this.board.toJson()));
        roundJson.put("numRound", this.numRound);
        roundJson.put("flagModifiable", this.flagModifiable);

        roundJson.put("playerCardsMap", playerCardsMapJson);

        return roundJson;
    }
}

