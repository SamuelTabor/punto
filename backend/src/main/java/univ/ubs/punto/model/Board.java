package univ.ubs.punto.model;

import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import univ.ubs.punto.Config;
import univ.ubs.punto.exceptions.RoundException;
import univ.ubs.punto.service.GameService;

import java.awt.*;
import java.util.*;

public class Board {
    private static final GameService gameService = GameService.getInstance();
    private static final Logger logger = LoggerFactory.getLogger(Board.class);
    private final long gameId;
    private final int roundId;
    private final ArrayList<ArrayList<Cell>> board;

    /**
     * Represent the playable area inside the board: [xMax, yMax, xMin, yMin]
     */
    private int[] border = new int[]{Config.BOARD_SIZE - 1, Config.BOARD_SIZE - 1, Config.BOARD_SIZE - 1, Config.BOARD_SIZE - 1};
    private final Set<Point> playableCells = new HashSet<>();

    /**
     * Flag to define if the game is modifiable without restrictions any by the app
     **/
    private final boolean flagModifiable;

    //****************************************************************************************************************//
    //************************************************** CONSTRUCTORS ***********************************************//

    public Board(long gameId, int roundId, boolean flagModifiable) {
        if (gameId < 0) {
            throw new IllegalArgumentException("gameId must be positive: %d".formatted(gameId));
        }
        if (roundId < 0) {
            throw new IllegalArgumentException("roundId must be positive: %d".formatted(roundId));
        }

        this.board = initBoard();
        this.gameId = gameId;
        this.roundId = roundId;
        this.playableCells.add(new Point(Config.BOARD_SIZE - 1, Config.BOARD_SIZE - 1));
        this.flagModifiable = flagModifiable;
        logger.debug("Board created with gameId: %d and roundId: %d".formatted(gameId, roundId));
    }

    //****************************************************************************************************************//
    //************************************************** GETTERS ****************************************************//

    public ArrayList<ArrayList<Cell>> getBoard() {
        return this.board;
    }

    public long getGameId() {
        return this.gameId;
    }

    public int getRoundId() {
        return this.roundId;
    }

    public boolean getFlagModifiable() {
        return this.flagModifiable;
    }

    public Set<Point> getPlayableCells() {
        return this.playableCells;
    }


    //****************************************************************************************************************//
    //************************************************** SETTERS ****************************************************//

    /*
    public void setBorder(int[] border) {
        if (border == null) throw new IllegalArgumentException("border must not be null");
        if (!this.flagModifiable) throw new IllegalArgumentException("The board is not modifiable");
        if (border.length != 4) throw new IllegalArgumentException("border must have a length of 4: %d".formatted(border.length));
        if (border[0] < 0 || border[0] > (Config.INNER_BOARDER * 2 - 1)) throw new IllegalArgumentException("border[0] must be between 1 and %d: %d".formatted((Config.INNER_BOARDER * 2 - 1), border[0]));
        if (border[1] < 0 || border[1] > (Config.INNER_BOARDER * 2 - 1)) throw new IllegalArgumentException("border[1] must be between 1 and %d: %d".formatted((Config.INNER_BOARDER * 2 - 1), border[1]));
        if (border[2] < 0 || border[2] > (Config.INNER_BOARDER * 2 - 1)) throw new IllegalArgumentException("border[2] must be between 1 and %d: %d".formatted((Config.INNER_BOARDER * 2 - 1), border[2]));
        if (border[3] < 0 || border[3] > (Config.INNER_BOARDER * 2 - 1)) throw new IllegalArgumentException("border[3] must be between 1 and %d: %d".formatted((Config.INNER_BOARDER * 2 - 1), border[3]));

        this.border = border;
        logger.debug("Board's border updated");
    }

     */

    //****************************************************************************************************************//
    //************************************************** METHODS ****************************************************//


    private ArrayList<ArrayList<Cell>> initBoard() {
        ArrayList<ArrayList<Cell>> board = new ArrayList<>();

        // Boucle for pour parcourir les lignes
        for (int i = 0; i < (Config.BOARD_SIZE * 2 - 1); i++) {
            board.add(new ArrayList<>());
            //Boucle for pour parcourir les colones
            for (int j = 0; j < (Config.BOARD_SIZE * 2 - 1); j++) {
                board.get(i).add(new Cell());
                logger.debug("Cell created at row: %d and col: %d".formatted(i, j));
            }
        }
        logger.debug("Board initialized");
        return board;
    }

    /**
     * Update the board with the card given in parameter
     *
     * @param row    the row of the card
     * @param col    the column of the card
     * @param card   the card to add
     * @param player the player who played the card
     */
    public boolean updateBoard(Card card, Player player, int row, int col) {
        if (row < 0 || row > (Config.BOARD_SIZE * 2 - 2))
            throw new IllegalArgumentException("row must be between 1 and %d: %d".formatted((Config.BOARD_SIZE * 2 - 1), row));
        if (col < 0 || col > (Config.BOARD_SIZE * 2 - 2))
            throw new IllegalArgumentException("col must be between 1 and %d: %d".formatted((Config.BOARD_SIZE * 2 - 1), col));
        if (card == null) throw new IllegalArgumentException("card must not be null");
        if (player == null) throw new IllegalArgumentException("player must not be null");

        //If the next move is inside the playable area
        if (getPlayableCells().contains(new Point(row, col))) {
            // Try to play the move
            if (this.getBoard().get(row).get(col).updateCell(card, player)) {
                logger.debug("Player %s played %s at row: %d and col: %d".formatted(player, card, row + 1, col + 1));
                //Update the inner border
                updateBorder(row, col);

                //Update playable area
                updatePlayableCells();
            
                //look for winner
                ArrayList<Player> winners = lookForWinner();

                if (!winners.isEmpty()) {
                    try {
                        //Set round winner and change the status to finished
                        gameService.getGameByID(this.gameId).getRounds().get(this.roundId).setWinners(winners);
                        gameService.getGameByID(this.gameId).getRounds().get(this.roundId).setRoundStatus(GameStatus.FINISHED);
                        logger.debug("Player %s won the round %d of the game %d".formatted(winners, this.roundId, this.gameId));
                    } catch (RoundException e) {
                        logger.error("Error while setting the winner of the round %d of the game %d: %s".formatted(this.roundId, this.gameId, e.getMessage()));
                    } catch (IllegalArgumentException e) {
                        logger.error("Error while setting the status of the round %d of the game %d: %s".formatted(this.roundId, this.gameId, e.getMessage()));
                    }
                }
                return true;
            }
            logger.debug("Player %s can't play %s at row: %d and col: %d because his card is lower than the card he's trying to replace".formatted(player, card, row + 1, col + 1));
            return false;
        }
        logger.debug("Player %s can't play %s at row: %d and col: %d because cell is not in the playable area".formatted(player, card, row + 1, col + 1));
        return false;
    }

    public void updateBorder(int row, int col) {
        //Update the inner border
        this.border[0] = Math.max(this.border[0], row);
        this.border[1] = Math.max(this.border[1], col);
        this.border[2] = Math.min(this.border[2], row);
        this.border[3] = Math.min(this.border[3], col);
        logger.debug("Board's border updated");
    }

    public void updatePlayableCells() {
        Set<Point> newPoints = new HashSet<>();
        for (Point point : this.playableCells) {
            //Vheck if it's a card
            if (this.getBoard().get(point.x).get(point.y).getCard() != null) {
                //Check a square of 3x3 around the card
                for (int i = point.x - 1; i <= point.x + 1; i++) {
                    for (int j = point.y - 1; j <= point.y + 1; j++) {
                        //Emulate the next move
                        if (emulateNextMove(i, j)) {
                            //If the next move is inside the border, add it to the new playable area
                            newPoints.add(new Point(i, j));
                        }
                    }
                }
            }
        }

        //Update the playable area
        this.playableCells.clear();
        this.playableCells.addAll(newPoints);
        logger.debug("Board's playable area updated");
    }

    /**
     * Emulate a move and check the new border updated
     * @param row the row of the card
     * @param col the column of the card
     * @return true if the newt move can be performed, false otherwise
     */
    public boolean emulateNextMove(int row, int col) {
        int xMax = Math.max(this.border[0], row);
        int yMax = Math.max(this.border[1], col);
        int xMin = Math.min(this.border[2], row);
        int yMin = Math.min(this.border[3], col);

        return xMax - xMin < Config.BOARD_SIZE && yMax - yMin < Config.BOARD_SIZE;
    }

    public ArrayList<Player> lookForWinner() {
        ArrayList<Player> winners = new ArrayList<>();
        //Check if the column border is big enough to align x cards to win
        //If it is, check for a winner in the columns
        Player winner = (this.border[0] - this.border[2] + 1 >= Config.CARD_TO_ALIGN_TO_WIN) ? checkCols() : null;
        if (winner != null) {
            winners.add(winner);
            winner.increaseRoundScore();
        }

        //Check if the row border is big enough to align x cards to win
        //If it is, check for a winner in the rows
        winner = (this.border[1] - this.border[3] + 1 >= Config.CARD_TO_ALIGN_TO_WIN) ? checkRows() : null;
        if (winner != null) {
            winners.add(winner);
            winner.increaseRoundScore();
        }

        //Check if the diagonal border is big enough to align x cards to win
        //If it is, check for a winner in the diagonals
        winner = ((this.border[1] - this.border[3] + 1 >= Config.CARD_TO_ALIGN_TO_WIN) &&
                (this.border[0] - this.border[2] + 1 >= Config.CARD_TO_ALIGN_TO_WIN)) ? checkDiags() : null;
        if (winner != null) {
            winners.add(winner);
            winner.increaseRoundScore();
        }
        return winners;
    }

    public Player checkRows() {
        int count = 0;
        Color lastColor = null;
        int xMax = this.border[0];
        int yMax = this.border[1];
        int xMin = this.border[2];
        int yMin = this.border[3];

        //Place the cursor at the left of the row that contains at least 1 card
        for (int row = xMin; row <= xMax; row++) {
            //Move the cursor through the row
            //1st condition : Il reste assez de case pour recommencer une suite de carte en ayant une chance de gagner.
            //2nd condition : le nombre de cartes de même couleur à trouver est inferior ou eagle au nombre de case à parcourir restante
            for (int col = yMin; (yMax - col + 1 >= Config.CARD_TO_ALIGN_TO_WIN) ||
                    (Config.CARD_TO_ALIGN_TO_WIN - count <= yMax - col + 1); col++) {
                //Get the color of the card
                Cell cell = this.getBoard().get(row).get(col);
                //If there is a card
                if (cell.getCard() != null) {
                    //If the color is the same as the last one, increment the counter
                    if (cell.getCard().getCardColor().equals(lastColor)) {
                        count++;
                        //if count matches the number of cards to align to win, return the winner
                        if (count == Config.CARD_TO_ALIGN_TO_WIN) {
                            return cell.getPlayer();
                        }
                    } else {
                        //If the color is different, reset the counter and update the last color
                        count = 1;
                        lastColor = cell.getCard().getCardColor();
                    }
                }
            }
        }
        return null;
    }

    public Player checkCols() {
        int count = 0;
        Color lastColor = null;
        int xMax = this.border[0];
        int yMax = this.border[1];
        int xMin = this.border[2];
        int yMin = this.border[3];

        //Place the cursor at the top of the column that contains at least 1 card
        for (int col = yMin; col <= yMax; col++) {
            //Move the cursor through the column
            //1st condition: There are enough cells left to start a new sequence of cards with a chance of winning.
            //2nd condition: the number of cards of the same color to find is less than or equal to the number of remaining cells to traverse
            for (int row = xMin; (xMax - row + 1 >= Config.CARD_TO_ALIGN_TO_WIN) ||
                    (Config.CARD_TO_ALIGN_TO_WIN - count <= xMax - row + 1); row++) {
                //Get the color of the card
                Cell cell = this.getBoard().get(row).get(col);
                //If there is a card
                if (cell.getCard() != null) {
                    //If the color is the same as the last one, increment the counter
                    if (cell.getCard().getCardColor().equals(lastColor)) {
                        count++;
                        //if count matches the number of cards to align to win, return the winner
                        if (count == Config.CARD_TO_ALIGN_TO_WIN) {
                            return cell.getPlayer();
                        }
                    } else {
                        //If the color is different, reset the counter and update the last color
                        count = 1;
                        lastColor = cell.getCard().getCardColor();
                    }
                }
            }
        }
        return null;
    }

    public Player checkDiags() {

        //TODO: fixe diag haut gauche bas droite

        int count = 0;
        Color lastColor = null;
        int xMax = this.border[0];
        int yMax = this.border[1];
        int xMin = this.border[2];
        int yMin = this.border[3];

        //Look for winner in diags from the top left corner to the bottom right corner
        for (int diag = xMin; xMax - diag + 1 >= Config.CARD_TO_ALIGN_TO_WIN; diag++) {
            for (int row = diag, col = yMin; row <= xMax && col <= yMax; row++, col++) {
                Cell cell = this.getBoard().get(row).get(col);
                if (cell.getCard() != null) {
                    if (cell.getCard().getCardColor().equals(lastColor)) {
                        count++;
                        if (count == Config.CARD_TO_ALIGN_TO_WIN) {
                            return cell.getPlayer();
                        }
                    } else {
                        count = 1;
                        lastColor = cell.getCard().getCardColor();
                    }
                }
            }
        }

        count = 0;
        lastColor = null;

        //Look for winner in diags from the top right corner to the bottom left corner
        for (int diag = xMin; xMax - diag + 1 >= Config.CARD_TO_ALIGN_TO_WIN; diag++) {
            for (int row = diag, col = yMax; row <= xMax && col >= yMin; row++, col--) {
                Cell cell = this.getBoard().get(row).get(col);
                if (cell.getCard() != null) {
                    if (cell.getCard().getCardColor().equals(lastColor)) {
                        count++;
                        if (count == Config.CARD_TO_ALIGN_TO_WIN) {
                            return cell.getPlayer();
                        }
                    } else {
                        count = 1;
                        lastColor = cell.getCard().getCardColor();
                    }
                }
            }
        }

        return null;
    }

    //****************************************************************************************************************//
    //********************************************* toString() & toJson() ********************************************//

    public String toString() {
        StringBuilder res = new StringBuilder();
        for (ArrayList<Cell> cells : this.board) {
            for (Cell cell : cells) {
                Card card = cell.getCard();
                res.append("| ").append(card != null ? card.getCardNumber() + String.valueOf(card.getCardColor().toString().charAt(0)) : "  ").append(" ");
            }
            res.append("|\n");
        }
        return res.toString();
    }

    public JSONObject toJson() {
        JSONArray boardJson = new JSONArray();
        for (ArrayList<Cell> row : this.board) {
            JSONArray rowJson = new JSONArray();
            for (Cell cell : row) {
                JSONObject cellJson = new JSONObject(cell.toJson());
                rowJson.put(cellJson);
            }
            boardJson.put(rowJson);
        }

        JSONObject finalJson = new JSONObject();
        finalJson.put("board", boardJson);
        finalJson.put("gameId", this.gameId);
        finalJson.put("roundId", this.roundId);
        finalJson.put("flagModifiable", this.flagModifiable);

        return finalJson;
    }
}
