package univ.ubs.punto.model;

import org.json.JSONObject;
import org.slf4j.LoggerFactory;
import org.slf4j.Logger;


public class Cell {
    private final Logger logger = LoggerFactory.getLogger(Cell.class);
    private Card card;
    private Player player;

    //****************************************************************************************************************//
    //************************************************** CONSTRUCTORS ***********************************************//

    public Cell(Card card, Player player) {
        if (card == null) throw new IllegalArgumentException("card must not be null");
        if (player == null) throw new IllegalArgumentException("player must not be null");
        this.card = card;
        this.player = player;
        logger.debug("Cell created");
    }

    public Cell() {
        this.card = null;
        this.player = null;
        logger.debug("Cell empty created");
    }

    //****************************************************************************************************************//
    //************************************************** GETTERS ****************************************************//

    public Card getCard() {
        return this.card;
    }

    public Player getPlayer() {
        return this.player;
    }

    //****************************************************************************************************************//
    //************************************************** SETTERS ****************************************************//

    public void setCard(Card card) {
        if (card == null) throw new IllegalArgumentException("card must not be null");
        this.card = card;
        logger.debug("Cell's card updated");
    }

    public void setPlayer(Player player) {
        if (player == null) throw new IllegalArgumentException("player must not be null");
        this.player = player;
        logger.debug("Cell's player updated");
    }

    //****************************************************************************************************************//
    //************************************************** METHODS ****************************************************//

    public boolean isEmpty() {
        return this.card == null && this.player == null;
    }

    public boolean updateCell(Card card, Player player) {
        if (card == null) throw new IllegalArgumentException("card must not be null");
        if (player == null) throw new IllegalArgumentException("player must not be null");

        if (this.isEmpty()) {
            this.card = card;
            this.player = player;
            logger.debug("Cell updated");
            return true;
        } else {
            if (this.card.compareTo(card) < 0) {
                this.card = card;
                this.player = player;
                logger.debug("Cell updated from %s to %s".formatted(this.card, card));
                return true;
            }
        }
        return false;
    }

    //****************************************************************************************************************//
    //********************************************* toString() & toJson() ********************************************//

    public String toString() {
        String res = "";
        res += "Card: " + ((this.card != null) ? this.card : "null") + ", ";
        res += "Player: " + ((this.player != null) ? this.player : "null");
        return res;
    }

    public JSONObject toJson() {
        return new JSONObject()
                .put("card", this.card)
                .put("player", this.player);
    }
}
