package univ.ubs.punto.model;

import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import univ.ubs.punto.Config;
import univ.ubs.punto.database.DatabaseFactory;
import univ.ubs.punto.exceptions.GameException;
import univ.ubs.punto.exceptions.RoundException;
import univ.ubs.punto.service.GameService;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Random;

public class Game {
    /**
     * Represents the logger of the class
     */
    private static final Logger logger = LoggerFactory.getLogger(Game.class);

    /**
     * ID of the game
     */
    private long gameId;

    /**
     * Number of rounds
     */
    private int nbRounds;

    /**
     * List of players
     */
    private ArrayList<Player> players;

    /**
     * List of rounds
     */
    private final ArrayList<Round> rounds;

    /**
     * Status of the game: WAITING, STARTED or FINISHED
     */
    private GameStatus gameStatus;

    /**
     * Winner of the game
     */
    private ArrayList<Player> winners;

    /**
     * Current round
     */
    private Round currentRound;

    /**
     * the description of the game
     */
    private String gameDescription;

    /**
     * Flag to define if the game is modifiable without restrictions any by the app
     **/
    private final boolean flagModifiable;

    //****************************************************************************************************************//
    //************************************************** CONSTRUCTORS ***********************************************//


    /**
     * Constructor of the class Game
     *
     * @param players         the list of players
     * @param nbRounds        the number of rounds
     * @param gameDescription the description of the game
     * @param flagModifiable  the flag to define if the game is modifiable without restrictions any by the app
     * @throws IllegalArgumentException if the number of rounds is <= 0
     */
    public Game(ArrayList<Player> players, int nbRounds, String gameDescription, boolean flagModifiable) {
        //test parameters
        if (nbRounds < Config.MIN_ROUNDS) {
            throw new IllegalArgumentException("Number of rounds cannot be negative");
        }

        this.gameId = GameService.generateID();
        this.nbRounds = nbRounds;
        this.players = players != null ? players : new ArrayList<>();
        this.gameStatus = GameStatus.WAITING;
        this.winners = new ArrayList<>();
        this.currentRound = null;
        this.rounds = new ArrayList<>();
        this.gameDescription = gameDescription != null ? gameDescription : "No description provided";
        this.flagModifiable = flagModifiable;

        logger.debug(String.format("Game %d created with %d players", this.gameId, this.players.size()));
    }

    //****************************************************************************************************************//
    //************************************************** GETTERS ****************************************************//

    public long getGameId() {
        return this.gameId;
    }

    public int getNbRounds() {
        return this.nbRounds;
    }

    public ArrayList<Player> getPlayers() {
        return this.players;
    }

    public ArrayList<Round> getRounds() {
        return this.rounds;
    }

    /**
     * Get a round by its ID
     *
     * @param roundId the ID of the round
     * @return the round
     * @throws IndexOutOfBoundsException if the round ID is not between 0 and the number of rounds
     * @throws GameException             if the game is not started
     */
    public Round getRound(int roundId) throws IndexOutOfBoundsException, GameException {
        if (this.gameStatus == GameStatus.WAITING)
            throw new GameException("Cannot get a round of a game that haven't started because tle list of rounds isn't initialised");
        if (roundId < 0 || roundId >= this.rounds.size())
            throw new IndexOutOfBoundsException("Round ID not acceptable: " + roundId + ". Must be between 0 and " + (this.rounds.size() - 1));

        if (roundId == this.rounds.get(roundId).getRoundId()) {
            return this.rounds.get(roundId);
        } else {
            for (Round round : this.rounds) {
                if (round.getRoundId() == roundId) {
                    logger.warn(String.format("Round %d not found at the right index : %d instead of %d", roundId, this.rounds.indexOf(round), roundId));
                    return round;
                }
            }
        }
        return null;
    }

    public GameStatus getGameStatus() {
        return this.gameStatus;
    }

    public ArrayList<Player> getGameWinners() {
        return this.winners;
    }

    public Round getCurrentRound() {
        return this.currentRound;
    }

    public String getGameDescription() {
        return this.gameDescription;
    }

    public boolean getFlagModifiable() {
        return this.flagModifiable;
    }

    //****************************************************************************************************************//
    //************************************************** SETTERS ****************************************************//

    /**
     * Setter of the game ID. The game ID cannot be null
     *
     * @param id the id of the game
     * @throws IllegalArgumentException if the id is null
     * @throws GameException            if the game is not modifiable
     */
    public void setGameId(Integer id) throws GameException {
        if (id == null) throw new IllegalArgumentException("Game id cannot be null");

        if (!this.flagModifiable) throw new GameException("Cannot modify the id of a game that is not modifiable");

        logger.debug(String.format("Game %d id updated to %d", this.gameId, id));
        this.gameId = id;
    }

    /**
     * Set the number of rounds for the game. The number of rounds cannot be <= 0
     *
     * @param nbRounds the number of rounds
     * @throws IllegalArgumentException if the number of rounds is <= 0
     * @throws GameException            if the game is already started or finished
     */
    public void setNbRounds(int nbRounds) throws IllegalArgumentException, GameException {
        if (nbRounds < Config.MIN_ROUNDS) {
            throw new IllegalArgumentException("Number of rounds cannot be negative");
        }
        if (this.gameStatus != GameStatus.WAITING)
            throw new GameException("Cannot set the number of rounds of a game that is already started or finished");

        this.nbRounds = nbRounds;
        logger.debug(String.format("Game %d number of rounds updated to %d", this.gameId, this.nbRounds));
    }

    /**
     * Set the status of the game. The status can be WAITING, STARTED or FINISHED
     *
     * @param status the status of the game
     */
    public void setGameStatus(GameStatus status) throws IllegalArgumentException, GameException {
        if (status == null) {
            throw new IllegalArgumentException("Game status cannot be null");
        }
        //if the game if not fully modifiable
        if (!this.flagModifiable) {
            //WAITING --> STARTED: KO if no rounds
            if (this.gameStatus == GameStatus.WAITING && status == GameStatus.STARTED && this.rounds.isEmpty()) {
                throw new GameException("Cannot set a game to STARTED if it doesn't have any rounds");
            }
            //WAITING --> FINISHED: KO
            if (this.gameStatus == GameStatus.WAITING && status == GameStatus.FINISHED) {
                throw new GameException("Cannot set a game to FINISHED if it haven't started yet");
            }
            //STARTED --> WAITING: KO
            if (this.gameStatus == GameStatus.STARTED && status == GameStatus.WAITING) {
                throw new GameException("Cannot set a game to WAITING if it is already started");
            }
            //STARTED --> FINISHED: KO if all rounds are not finished
            if (status == GameStatus.FINISHED && this.gameStatus == GameStatus.STARTED && !this.allRoundsFinished()) {
                throw new GameException("Cannot set a game to FINISHED if all the rounds are not finished");
            }
            //STARTED --> FINISHED: KO because no winner
            if (status == GameStatus.FINISHED && this.winners.isEmpty()) {
                throw new GameException("Cannot set a game to FINISHED if it has no winner");
            }
            //FINISHED --> WAITING | STARTED: OK
            if (this.gameStatus == GameStatus.FINISHED && (status == GameStatus.WAITING || status == GameStatus.STARTED)) {
                throw new GameException(String.format("Cannot set a game to %s or STARTED if it is already finished", status));
            }
            this.gameStatus = status;
            logger.debug(String.format("Game %d status updated to %s", this.gameId, this.gameStatus));
        } else {
            //force the modification
            logger.debug(String.format("Game %d status updated from %s to %s", this.gameId, this.gameStatus, status));
            this.gameStatus = status;
        }
    }

    /**
     * Set the winners of the game. The winners are the players that has won the most rounds
     * There can be multiple winners if they have the same number of wins
     * @return the winner of the game, null if all rounds aren't finished
     * @throws IllegalArgumentException if the game status is WAITING
     */
    public ArrayList<Player> setGameWinners() throws GameException {
        if (this.gameStatus == GameStatus.WAITING)
            throw new GameException("Cannot set a winner to a game that haven't started");

        //check all rounds to see if they are all finished
        boolean allRoundsFinished = this.allRoundsFinished();
        //search the player which is the most present in rounds' winners
        if (allRoundsFinished) {
            //max wins counter
            int max = 0;
            //for each player, count how many times they win a round
            for (Player player : this.players) {
                int nbWins = this.countPlayerWin(player);
                //if the number of wins for the current player is higher than the max, update the max and the winner
                if (nbWins > max) {
                    max = nbWins;
                    this.winners.clear();
                    this.winners.add(player);
                } else if (nbWins == max) {
                    //Manege equality
                    this.winners.add(player);
                }
            }

            for (Player winner : winners) {
                winner.increaseGameScore();
            }

            logger.debug(String.format("Game %d winner updated to %s", this.gameId, this.winners));
            return this.winners;
        }
        logger.warn(String.format("Game %d cannot have a winner because all rounds are not finished", this.gameId));
        return new ArrayList<>();
    }

    /**
     * Set the winners of the game. It can perform the action only if the game's flagModifiable is true.
     *
     * @param winners the winner of the game
     * @throws IllegalArgumentException if the winner is null
     * @throws GameException            if the game is not modifiable
     */
    public void setGameWinners(ArrayList<Player> winners) throws GameException {
        if (!this.flagModifiable)
            throw new GameException("Cannot set a specific winner to a game that is not modifiable.");
        if (winners == null) throw new IllegalArgumentException("Game winner cannot be null");

        this.winners = winners;
        logger.debug(String.format("Game %d winner updated to %s", this.gameId, this.winners));
    }

    /**
     * Set the current round of the game. The current round is the round that is currently played
     *
     * @param currentRound the current round
     * @throws IllegalArgumentException if the current round is null, or if the round is not in the list of rounds
     * @throws GameException            if the game status is WAITING, if the previous round is not finished, or if the
     *                                  current round is not in the list of rounds
     */
    public void setCurrentRound(Round currentRound) throws IllegalArgumentException, GameException {
        if (currentRound == null) throw new IllegalArgumentException("Current round cannot be null");
        if (currentRound.equals(this.currentRound)) {
            logger.warn(String.format("Game %d current round is already %d", this.gameId, currentRound.getRoundId()));
            return;
        }
        if (this.gameStatus == GameStatus.WAITING)
            throw new GameException("Cannot set a current round to a game that haven't started");

        //check if the round is in the list of rounds
        if (!this.rounds.contains(currentRound))
            throw new GameException("Cannot set a current round that is not in the list of rounds");

        //check if the previous round is finished
        if (this.currentRound != null && this.currentRound.getRoundStatus() != GameStatus.FINISHED) {
            throw new GameException("Cannot set a current round if the previous round is not finished");
        }
        this.currentRound = currentRound;
        logger.debug(String.format("Game %d current round updated to %d", this.gameId, this.currentRound.getRoundId()));
    }

    public void setGameDescription(String description) {
        if (description == null) throw new IllegalArgumentException("Game description cannot be null");

        this.gameDescription = description;
        logger.debug(String.format("Game %s description updated with : %s", this.gameId, this.gameDescription));
    }

    public void setPlayers(ArrayList<Player> players) throws GameException {
        if (players == null) throw new IllegalArgumentException("Game description cannot be null");
        if (this.gameStatus != GameStatus.WAITING) throw new GameException("Cannot set players of a game that is already started or finished");
        this.players = players;
        logger.debug(String.format("Game %s description updated with : %s", this.gameId, this.gameDescription));
    }
    //****************************************************************************************************************//
    //************************************************** METHODS ****************************************************//

    /**
     * Check if all the rounds of the game are finished
     *
     * @return true if all the rounds are finished, false otherwise
     */
    public boolean allRoundsFinished() {
        if (this.gameStatus.equals(GameStatus.WAITING)) {
            logger.warn(String.format("Game %d cannot have all rounds finished because it haven't started", this.gameId));
            return false;
        } else {
            boolean allRoundsFinished = true;
            for (Round round : this.rounds) {
                if (round.getRoundStatus() != GameStatus.FINISHED) {
                    allRoundsFinished = false;
                    break;
                }
            }
            logger.debug(String.format("Game %d all rounds finished : %b", this.gameId, allRoundsFinished));
            return allRoundsFinished;
        }
    }

    /**
     * Count the number of wins for a player in the game
     *
     * @param player the player
     * @return the number of wins for the player
     */
    public int countPlayerWin(Player player) {
        int nbWins = 0;
        for (Round round : this.rounds) {
            if (round.getRoundStatus() == GameStatus.FINISHED && round.getWinners().contains(player)) nbWins++;
        }
        logger.debug(String.format("Game %d player %s wins : %d", this.gameId, player.getNom(), nbWins));
        return nbWins;
    }

    /**
     * Add a player to a game.
     * A game can be played with a maximum of four players.
     * The player going to be added should not yet be in the list of players
     *
     * @param player the player to add
     * @return true if the player was added, false otherwise
     * @throws IllegalArgumentException if the player is null
     * @throws GameException            if the game is already started or finished
     */
    public boolean addPlayer(Player player) throws IllegalArgumentException, GameException {
        if (player == null) {
            throw new IllegalArgumentException("Player cannot be null");
        }
        if (this.gameStatus != GameStatus.WAITING) {
            throw new GameException("Cannot add a player to a game that is already started or finished");
        }

        if (this.players.size() >= Config.MAX_PLAYERS) {
            logger.error(String.format("Cannot add a player to a game that already have %d players", Config.MAX_PLAYERS));
            return false;
        }

        if (!this.players.contains(player)) {
            this.players.add(player);
            logger.debug(String.format("Player %s added to game %d", player.getNom(), this.gameId));
            return true; // Joueur ajouté avec succès
        }

        logger.error("Cannot add a player that is already in the list of players");
        return false; // Le joueur était déjà dans la liste
    }


    /**
     * Remove a player from the game.
     *
     * @param player the player
     * @return true if the player has been removed, false otherwise
     */
    public boolean removePlayer(Player player) {
        if (player == null) {
            throw new IllegalArgumentException("Player cannot be null");
        }
        if (this.gameStatus != GameStatus.WAITING) {
            throw new IllegalArgumentException("Cannot remove a player from a game that is already started or finished");
        }

        logger.debug(String.format("Trying to remove player %s from game %d", player.getNom(), this.gameId));
        return this.players.remove(player);
    }

    /**
     * Start the game. The game can be started only if the number of players is between {@value Config#MIN_PLAYERS} and {@value Config#MAX_PLAYERS}, the number of rounds
     * is higher than 0, and the status of the game is WAITING
     *
     * @throws GameException if the game cannot be started
     */
    public void startGame() throws GameException, RoundException {
        //check if the game can be started
        if (this.players.size() < Config.MIN_PLAYERS || this.players.size() > Config.MAX_PLAYERS) {
            throw new GameException(String.format("Number of players not acceptable: %d. Must be between %d and %d", this.players.size(), Config.MIN_PLAYERS, Config.MAX_PLAYERS));
        }
        //always true because the number of rounds is checked in the constructor
        if (this.nbRounds < Config.MIN_ROUNDS) {
            throw new GameException(String.format("Number of rounds not acceptable: %d. Must be higher than %d", this.nbRounds, Config.MIN_ROUNDS));
        }
        if (this.gameStatus != GameStatus.WAITING) {
            throw new GameException("Game status not acceptable: " + this.gameStatus.toString() + ". Must be WAITING");
        }
        if (!this.winners.isEmpty()) {
            throw new GameException("A game that haven't started cannot have a winner : " + this.winners);
        }
        if (this.currentRound != null) {
            throw new GameException("A game that haven't started cannot have a current round : " + this.currentRound);
        }

        //Initialisation of the game

        //Init all the rounds
        for (int i = 0; i < this.nbRounds; i++) {
            this.rounds.add(new Round(this, i, false));
        }
        logger.debug(String.format("Game %d started with %d rounds", this.gameId, this.nbRounds));
        //set the current round to the 1st occurrence
        this.currentRound = this.rounds.get(0);

        //insert the game in the database
        try {
            DatabaseFactory.getInstance().insert(this);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        logger.debug(String.format("Game %d current round updated to %d", this.gameId, this.currentRound.getRoundId()));
        //set the status of the game to STARTED
        this.gameStatus = GameStatus.STARTED;
        logger.debug(String.format("Game %d started", this.gameId));
        //shuffle the players
        this.shufflePlayers();
        //update the game
        this.updateGame();
    }

    /**
     * Shuffle the players in the game
     */
    public void shufflePlayers() {
        if (this.players == null || this.players.isEmpty()) {
            logger.error(String.format("Cannot shuffle players of game %d because there is no players", this.gameId));
        } else {
            long seed = System.nanoTime();
            Collections.shuffle(this.players, new Random(seed));
            logger.debug(String.format("Game %d players shuffled.", this.gameId));
        }
    }

    /**
     * Manage all the actions to update the game like change the current round, the winner, the status of the game, etc.
     */
    public void updateGame() throws GameException, RoundException {
        //if the game is not started, start it
        if (this.gameStatus == GameStatus.WAITING) {
            this.startGame();
        }

        //if game is already finished, do nothing
        if (this.gameStatus == GameStatus.FINISHED) {
            logger.warn(String.format("Game %d is already finished", this.gameId));
            return;
        }

        //if the current round is FINISHED, set the next round as current round
        if (this.currentRound.getRoundStatus() == GameStatus.FINISHED) {
            if (this.currentRound.getRoundId() < this.nbRounds - 1) {
                this.setCurrentRound(this.rounds.get(this.currentRound.getRoundId() + 1));
            } else {
                logger.warn(String.format("Game %d cannot set the next round as current round because it is the last round", this.gameId));
            }
        }

        //if the current round is not started, start it
        if (this.currentRound.getRoundStatus() == GameStatus.WAITING) {
            this.currentRound.setRoundStatus(GameStatus.STARTED);
        }

        //if all rounds are finished, set game winner and status to FINISHED
        if (this.allRoundsFinished()) {
            this.setGameWinners();
            this.setGameStatus(GameStatus.FINISHED);
        }

        //update the game in the database
        try {
            DatabaseFactory.getInstance().update(this);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    //****************************************************************************************************************//
    //********************************************* toString() & toJson() ********************************************//

    public String toString() {
        StringBuilder winnersNames = new StringBuilder();
        for (Player winner : winners) {
            winnersNames.append(winner.getNom()).append(", ");
        }

        // Remove the trailing comma and space
        if (!winnersNames.isEmpty()) {
            winnersNames.setLength(winnersNames.length() - 2);
        }

        return "Game " + this.gameId + "\n" +
                "Description: " + this.gameDescription + "\n" +
                "Players: " + this.players + "\n" +
                "Status: " + this.gameStatus + "\n" +
                "Winners: " + winnersNames.toString() + "\n" +
                "Rounds: " + this.rounds + "\n" +
                "Current round: " + ((this.currentRound != null) ? this.currentRound.getRoundId() : "not current round");
    }

    public JSONObject toJson() {
        JSONObject gameJson = new JSONObject();
        gameJson.put("gameId", this.gameId);
        gameJson.put("nbRounds", this.nbRounds);
        gameJson.put("gameStatus", this.gameStatus.toString());
        gameJson.put("gameDescription", this.gameDescription);
        gameJson.put("flagModifiable", this.flagModifiable);

        JSONArray playersJson = new JSONArray();
        for (Player player : this.players) {
            playersJson.put(new JSONObject(player.toJson()));
        }
        gameJson.put("players", playersJson);

        JSONArray roundsJson = new JSONArray();
        for (Round round : this.rounds) {
            roundsJson.put(new JSONObject(round.toJson()));
        }
        gameJson.put("rounds", roundsJson);

        if (!this.winners.isEmpty()) {
            JSONArray winnersJson = new JSONArray();
            for (Player winner : this.winners) {
                winnersJson.put(new JSONObject(winner.toJson()));
            }
            gameJson.put("winners", winnersJson);
        } else {
            gameJson.put("winners", JSONObject.NULL);
        }

        gameJson.put("currentRound", this.currentRound != null ? new JSONObject(this.currentRound.toJson()) : JSONObject.NULL);

        return gameJson;
    }
}
