package univ.ubs.punto.model;

import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import univ.ubs.punto.database.DatabaseFactory;
import univ.ubs.punto.service.GameService;

import java.util.Objects;

public class Player {
    private static final Logger logger = LoggerFactory.getLogger(Player.class);
    private long playerId;
    private String nom;
    private int gameScore;
    private int roundScore;
    //private final Set<Color> playerColors;
    //TODO: add color of cards and manage cardInit and shuffle deck

    //****************************************************************************************************************//
    //************************************************** CONSTRUCTORS ************************************************//

    /**
     * Constructor of the class Player
     * @param nom the name of the player
     */
    public Player(String nom, Long playerId) {
        //test parameters
        if (nom == null || nom.isEmpty()) throw new IllegalArgumentException("Player name cannot be null or empty");

        this.nom = nom;
        this.playerId = playerId != null ? playerId : GameService.generateID();
        this.gameScore = 0;
        this.roundScore = 0;

        //log info when a player is created with its name and id
        logger.debug(String.format("Player %s created with id %d", this.nom, this.playerId));
    }

    //****************************************************************************************************************//
    //************************************************** GETTERS *****************************************************//

    public String getNom(){
        return this.nom;
    }

    public long getPlayerId(){
        return this.playerId;
    }

    public int getGameScore() {
        return gameScore;
    }

    public int getRoundScore() {
        return roundScore;
    }

//****************************************************************************************************************//
    //************************************************** SETTERS *****************************************************//

    public void setNom(String nom){
        if (nom == null || nom.isEmpty()) throw new IllegalArgumentException("Player name cannot be null or empty");

        logger.debug(String.format("Player name set from %s to %s", this.nom, nom));
        this.nom = nom;
    }

    public void setPlayerId(long playerId){
        if (playerId < 0){
            throw new IllegalArgumentException("Player id cannot be negative");
        }

        this.playerId = playerId;
    }

    public void setGameScore(int gameScore) {
        this.gameScore = gameScore;
    }

    public void setRoundScore(int roundScore) {
        this.roundScore = roundScore;
    }

    //****************************************************************************************************************//
    //************************************************** METHODS *****************************************************//
    /**
     * Override the equals method to compare two players
     * @param o the object to compare
     * @return true if the two players are equals, false otherwise
     */
    @Override
    public boolean equals(Object o){
        if (o == this) return true;
        if (!(o instanceof Player player)) return false;

        return player.nom.equals(this.nom) && player.playerId == this.playerId;
    }

    /**
     * Override the hashCode method to compare two players
     * @return the hash code of the player
     */
    @Override
    public int hashCode() {
        return Objects.hash(nom, playerId);
    }

    public void increaseGameScore() {
        this.gameScore ++;
        try {
            DatabaseFactory.getInstance().update(this);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public void increaseRoundScore() {
        this.roundScore ++;
        try {
            DatabaseFactory.getInstance().update(this);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }


    //****************************************************************************************************************//
    //********************************************* toString() & toJson() ********************************************//

    public String toString(){
        return String.format("Player %s with id %d", this.nom, this.playerId);
    }

    public JSONObject toJson(){
        JSONObject playerJson = new JSONObject();
        playerJson.put("playerId", this.playerId);
        playerJson.put("nom", this.nom);
        return playerJson;
    }

}