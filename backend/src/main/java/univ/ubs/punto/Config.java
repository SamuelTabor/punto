package univ.ubs.punto;

import univ.ubs.punto.database.Database;

public final class Config {
    public static final int BOARD_SIZE = 6;
    public static final int MIN_ROUNDS = 1;
    public static final int MAX_PLAYERS = 4;
    public static final int MIN_PLAYERS = 2;
    public static final int NUMBER_OF_CARDS = 9;
    public static final int CARD_TO_ALIGN_TO_WIN = 5;
    public static final String FRONTEND_URL = "http://localhost:8080";
    public static Database DATABASE = Database.SQLITE;
    public static final String WORKING_DIRECTORY = System.getProperty("user.dir");

    private Config() {
        throw new AssertionError("Cannot instantiate this class");
    }
}
