package univ.ubs.punto.controller;


import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.springframework.web.bind.annotation.RequestBody;
import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import univ.ubs.punto.database.Database;
import univ.ubs.punto.database.DatabaseFactory;
import univ.ubs.punto.dto.GameDTO;
import univ.ubs.punto.dto.MoveDTO;
import univ.ubs.punto.exceptions.GameException;
import univ.ubs.punto.exceptions.PlayerException;
import univ.ubs.punto.exceptions.RoundException;
import univ.ubs.punto.model.Card;
import univ.ubs.punto.model.Color;
import univ.ubs.punto.model.Game;
import univ.ubs.punto.model.Player;
import univ.ubs.punto.requestObject.PlayerRequest;
import univ.ubs.punto.service.GameService;
import univ.ubs.punto.requestObject.GameRequest;

import java.util.ArrayList;

@RestController
@RequestMapping("/punto/local")
public class LocalGameController {
    private static final Logger logger = LoggerFactory.getLogger(LocalGameController.class);
    private static final GameService gameService = GameService.getInstance();


    @GetMapping("/games")
    @Operation(summary = "Get liste of all Games", description = "Get the list of games with less informations")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Successfully retrieved the list of games",
                    content = @Content(
                            mediaType = "application/json",
                            schema = @Schema(implementation = GameDTO[].class)
                    )),
            @ApiResponse(responseCode = "404", description = "Games not found",
                    content = @Content(
                            mediaType = "application/json")
            ),
            @ApiResponse(responseCode = "500", description = "Internal server error",
                    content = @Content(
                            mediaType = "application/json"
                    ))
    })
    public ResponseEntity<String> getGames() {
        ArrayList<GameDTO> games = new ArrayList<>();
        JSONObject res = new JSONObject();
        JSONArray gamesJSON = new JSONArray();
        try {
            for (Game game : gameService.getGames()) {
                games.add(new GameDTO(game));
                gamesJSON.put(new GameDTO(game).toJson());
            }
        } catch (Exception e) {
            res.put("message", e.getMessage());
            res.put("error", e.getCause());
            res.put("data", new JSONArray());
            return new ResponseEntity<>(res.toString(), null, HttpStatus.INTERNAL_SERVER_ERROR);
        }

        if (games.isEmpty()) {
            res.put("message", "Games not found");
            res.put("data", gamesJSON);
            res.put("error", new JSONObject());
            return new ResponseEntity<>(res.toString(), null, HttpStatus.NOT_FOUND);
        }
        res.put("message", "Successfully retrieved the list of games");
        res.put("data", gamesJSON);
        res.put("error", new JSONObject());
        return new ResponseEntity<>(res.toString(), null, HttpStatus.OK);
    }

    @GetMapping("/games/{gameId}")
    @Operation(summary = "Get a specific game", description = "Get the game matching the id parameter. Get more informations than in the list of games")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Successfully retrieved the game",
                    content = @Content(
                            mediaType = "application/json"
                    )
            ),
            @ApiResponse(responseCode = "404", description = "Game not found",
                    content = @Content(
                            mediaType = "application/json"
                    )),
            @ApiResponse(responseCode = "400", description = "Illegal argument",
                    content = @Content(
                            mediaType = "application/json"
                    )),
            @ApiResponse(responseCode = "500", description = "Internal server error",
                    content = @Content(
                            mediaType = "application/json"
                    ))
    })
    public ResponseEntity<String> getGameByID(@PathVariable long gameId) {
        Game game;
        JSONObject res = new JSONObject();
        try {
            game = gameService.getGameByID(gameId);
            if (game == null) throw new GameException.GameNotExistException("Game not found");
            res.put("message", "Successfully retrieved the game");
            res.put("data", new GameDTO(game).toJson());
            res.put("error", new JSONObject());
        } catch (IllegalArgumentException e) {
            res.put("message", e.getMessage());
            res.put("data", new JSONArray());
            res.put("error", e.getCause());
            return new ResponseEntity<>(e.getMessage(), null, HttpStatus.BAD_REQUEST);
        } catch (GameException.GameNotExistException e) {
            res.put("message", "No games found");
            res.put("error", new JSONObject());
            res.put("data", new JSONObject());
            return new ResponseEntity<>(res.toString(), null, HttpStatus.NOT_FOUND);
        } catch (Exception e) {
            res.put("message", e.getMessage());
            res.put("error", e.getCause());
            res.put("data", new JSONObject());
            return new ResponseEntity<>(res.toString(), null, HttpStatus.INTERNAL_SERVER_ERROR);
        }

        return new ResponseEntity<>(res.toString(), null, HttpStatus.OK);
    }

    @PostMapping("/games")
    @Operation(summary = "Create a game", description = "Create a new game")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Game successfully created",
                    content = @Content(
                            mediaType = "application/json"
                    )
            ),
            @ApiResponse(responseCode = "400", description = "Illegal arguments",
                    content = @Content(
                            mediaType = "application/json"
                    )
            ),
            @ApiResponse(responseCode = "500", description = "Internal server error",
                    content = @Content(
                            mediaType = "application/json"
                    )
            )
    })
    public ResponseEntity<String> createGame(@RequestBody GameRequest req) {
        Game game;
        JSONObject res = new JSONObject();
        try {
            if (req.getPlayers() == null || req.getPlayers().isEmpty()) throw new IllegalArgumentException("Players list cannot be null or empty");
            ArrayList<Player> players = gameService.getPlayers((ArrayList<Long>) req.getPlayers());
            game = new Game(players, req.getNbRounds(), req.getDescription(), false);
            gameService.addGame(game);
            game.startGame();
            res.put("message", "Game successfully created");
            res.put("data", new GameDTO(game).toJson());
            res.put("error", new JSONObject());
        } catch (IllegalArgumentException | PlayerException e) {
            res.put("message", e.getMessage());
            res.put("error", e.getCause());
            res.put("data", new JSONObject());
            return new ResponseEntity<>(res.toString(), null, HttpStatus.BAD_REQUEST);
        } catch (Exception e) {
            res.put("message", e.getMessage());
            res.put("error", e.getCause());
            res.put("data", new JSONObject());
            return new ResponseEntity<>(res.toString(), null, HttpStatus.INTERNAL_SERVER_ERROR);
        }

        return new ResponseEntity<>(res.toString(), null, HttpStatus.OK);
    }

    @PutMapping("/games/{gameId}")
    @Operation(summary = "Update a specific game", description = "Update the game with id matching parameter, can be performe only before the game has started")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Successfully updated the game",
                    content = @Content(
                            mediaType = "application/json"
                    )
            ),
            @ApiResponse(responseCode = "404", description = "Game not found",
                    content = @Content(
                            mediaType = "application/json"
                    )
            ),
            @ApiResponse(responseCode = "500", description = "Internal server error",
                    content = @Content(
                            mediaType = "application/json"
                    )
            )
    })
    public ResponseEntity<String> updateGame(@PathVariable long gameId, @RequestBody GameRequest req) {
        JSONObject res = new JSONObject();
        Game game;
        try {
            game = gameService.getGameByID(gameId);
            if (game == null) throw new GameException.GameNotExistException("Game not found");
            if (req.getNbRounds() != 0) game.setNbRounds(req.getNbRounds());
            if (req.getPlayers() != null) {
                ArrayList<Player> players = gameService.getPlayers((ArrayList<Long>) req.getPlayers());
                game.setPlayers(players);
            }
            if (req.getDescription() != null) game.setGameDescription(req.getDescription());
            res.put("message", "Game successfully updated");
            res.put("data", new GameDTO(game).toJson());
            res.put("error", new JSONObject());
        } catch (GameException.GameNotExistException e) {
            res.put("message", e.getMessage());
            res.put("error", e.getCause());
            res.put("data", new JSONObject());
            return new ResponseEntity<>(res.toString(), null, HttpStatus.NOT_FOUND);
        } catch(IllegalArgumentException | PlayerException.PlayerNotExistException e) {
            res.put("message", e.getMessage());
            res.put("error", e.getCause());
            res.put("data", new JSONObject());
            return new ResponseEntity<>(res.toString(), null, HttpStatus.BAD_REQUEST);
        } catch(GameException e) {
            res.put("message", e.getMessage());
            res.put("error", e.getCause());
            res.put("data", new JSONObject());
            return new ResponseEntity<>(res.toString(), null, HttpStatus.CONFLICT);
        }  catch (Exception e) {
            res.put("message", e.getMessage());
            res.put("error", e.getCause());
            res.put("data", new JSONObject());
            return new ResponseEntity<>(res.toString(), null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<>(res.toString(), null, HttpStatus.OK);
    }

    @DeleteMapping("/games/{gameId}")
    @Operation(summary = "Delete a specific game", description = "Delete the game with id matching parameter")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Successfully deleted the game",
                    content = @Content(
                            mediaType = "application/json"
                    )
            ),
            @ApiResponse(responseCode = "404", description = "Game not found",
                    content = @Content(
                            mediaType = "application/json"
                    )
            ),
            @ApiResponse(responseCode = "500", description = "Internal server error",
                    content = @Content(
                            mediaType = "application/json"
                    )
            )
    })
    public ResponseEntity<String> deleteGame(@PathVariable long gameId) {
        JSONObject res = new JSONObject();
        Game game;
        try {
            game = gameService.getGameByID(gameId);
            gameService.removeGame(game);
            res.put("message", "Game successfully deleted");
            res.put("data", new GameDTO(game).toJson());
            res.put("error", new JSONObject());
        } catch (IllegalArgumentException e) {
            res.put("message", e.getMessage());
            res.put("error", e.getCause());
            res.put("data", new JSONObject());
            return new ResponseEntity<>(res.toString(), null, HttpStatus.BAD_REQUEST);
        } catch (Exception e) {
            res.put("message", e.getMessage());
            res.put("error", e.getCause());
            res.put("data", new JSONObject());
            return new ResponseEntity<>(res.toString(), null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<>(res.toString(), null, HttpStatus.OK);
    }

    @PutMapping("/games/{gameId}/play")
    @Operation(summary = "Play a move", description = "Start the game matching the id parameter")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Successfully played the move",
                    content = @Content(
                            mediaType = "application/json"
                    )
            ),
            @ApiResponse(responseCode = "404", description = "Game not found",
                    content = @Content(
                            mediaType = "application/json"
                    )
            ),
            @ApiResponse(responseCode = "400", description = "Wrong arguments",
                    content = @Content(
                            mediaType = "application/json"
                    )
            ),
            @ApiResponse(responseCode = "409", description = "Move not allowed",
                    content = @Content(
                            mediaType = "application/json"
                    )
            ),
            @ApiResponse(responseCode = "500", description = "Internal server error",
                    content = @Content(
                            mediaType = "application/json"
                    )
            )
    })
    public ResponseEntity<String> playMove(@PathVariable long gameId, @RequestBody MoveDTO req) {
        JSONObject res = new JSONObject();
        Game game;

        //Gestion du fait que quand un joueur n'a plus de carte
        try {
            game = gameService.getGameByID(gameId);
            if (game == null) throw new GameException.GameNotExistException("Game not found");
            if (!game.getCurrentRound().play(gameService.getPlayerByID(req.getPlayerId()), new Card(req.getCardNumber(), Color.valueOf(req.getCardColor())), req.getRow(), req.getColumn()))
                throw new GameException.MoveNotAllowedException("Move not allowed");
            res.put("message", "Successfully played the move");
            res.put("data", new GameDTO(game).toJson());
            res.put("error", new JSONObject());
        } catch (IllegalArgumentException e) {
            res.put("message", e.getMessage());
            res.put("error", e.getCause());
            res.put("data", req.toJson());
            return new ResponseEntity<>(res.toString(), null, HttpStatus.BAD_REQUEST);
        } catch (RoundException | GameException.MoveNotAllowedException e) {
            res.put("message", e.getMessage());
            res.put("error", e.getCause());
            res.put("data", req.toJson());
            return new ResponseEntity<>(res.toString(), null, HttpStatus.CONFLICT);
        } catch (GameException.GameNotExistException e) {
            res.put("message", e.getMessage());
            res.put("error", e.getCause());
            res.put("data", req.toJson());
            return new ResponseEntity<>(res.toString(), null, HttpStatus.NOT_FOUND);
        } catch (Exception e) {
            res.put("message", e.getMessage());
            res.put("error", e.getCause());
            res.put("data", req.toJson());
            return new ResponseEntity<>(res.toString(), null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<>(res.toString(), null, HttpStatus.OK);
    }


    @PutMapping("/games/{gameId}/start")
    @Operation(summary = "Start a game", description = "Start a specific game")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Game successfully started",
                    content = @Content(
                            mediaType = "application/json"
                    )
            ),
            @ApiResponse(responseCode = "404", description = "Game not found",
                    content = @Content(
                            mediaType = "application/json"
                    )
            ),
            @ApiResponse(responseCode = "409", description = "The conditions are not met to start the game",
                    content = @Content(
                            mediaType = "application/json"
                    )
            ),
            @ApiResponse(responseCode = "500", description = "Internal server error",
                    content = @Content(
                            mediaType = "application/json"
                    )
            )
    })
    public ResponseEntity<String> startGame(@PathVariable long gameId) {
        JSONObject res = new JSONObject();
        Game game;

        try {
            game = gameService.getGameByID(gameId);
            if (game == null) throw new GameException.GameNotExistException("Game not found");
            game.startGame();
            res.put("message", "Game successfully started");
            res.put("data", new GameDTO(game).toJson());
            res.put("error", new JSONObject());
        } catch (GameException.GameNotExistException e) {
            res.put("message", e.getMessage());
            res.put("error", e.getCause());
            res.put("data", new JSONObject());
            return new ResponseEntity<>(res.toString(), null, HttpStatus.NOT_FOUND);
        } catch (GameException e) {
            res.put("message", e.getMessage());
            res.put("error", e.getCause());
            res.put("data", new JSONObject());
            return new ResponseEntity<>(res.toString(), null, HttpStatus.CONFLICT);
        } catch (Exception e) {
            res.put("message", e.getMessage());
            res.put("error", e.getCause());
            res.put("data", new JSONObject());
            return new ResponseEntity<>(res.toString(), null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<>(res.toString(), null, HttpStatus.OK);
    }

    @GetMapping("/players")
    @Operation(summary = "Get players list", description = "Get the list of players")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Successfully retrieved the list of players",
                    content = @Content(
                            mediaType = "application/json"
                    )
            ),
            @ApiResponse(responseCode = "500", description = "Internal server error",
                    content = @Content(
                            mediaType = "application/json"
                    )
            )
    })
    public ResponseEntity<String> getPlayers() {
        //return arraylist of players in json format
        JSONObject res = new JSONObject();
        JSONArray playersJSON = new JSONArray();
        ArrayList<Player> players = new ArrayList<>();
        try {
            players = gameService.getPlayers();

        } catch (Exception e) {
            res.put("message", e.getMessage());
            res.put("error", e.getCause());
            res.put("data", playersJSON);
            return new ResponseEntity<>(res.toString(), null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        res.put("message", "Successfully retrieved the list of players");
        res.put("data", players);

        return new ResponseEntity<>(res.toString(), null, HttpStatus.OK);
    }

    @PostMapping("/players")
    @Operation(summary = "Create a player", description = "Create a player")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Player successfully created",
                    content = @Content(
                            mediaType = "application/json"
                    )
            ),
            @ApiResponse(responseCode = "400", description = "Wrong arguments",
                    content = @Content(
                            mediaType = "application/json"
                    )
            ),
            @ApiResponse(responseCode = "409", description = "There is already a player with this name",
                    content = @Content(
                            mediaType = "application/json"
                    )
            ),
            @ApiResponse(responseCode = "500", description = "Internal server error",
                    content = @Content(
                            mediaType = "application/json"
                    )
            )
    })
    public ResponseEntity<String> createPlayer(@RequestBody PlayerRequest req) {
        Player player;
        JSONObject res = new JSONObject();
        try {
            player = new Player(req.getName(), null);
            gameService.addPlayer(player);

        } catch (IllegalArgumentException e) {
            res.put("message", e.getMessage());
            res.put("data", new JSONObject());
            res.put("error", e.getCause());
            return new ResponseEntity<>(res.toString(), null, HttpStatus.BAD_REQUEST);
        } catch (PlayerException.PlayerWithSameNameAlreadyExist e) {
            res.put("message", e.getMessage());
            res.put("data", new JSONObject());
            res.put("error", e.getCause());
            return new ResponseEntity<>(res.toString(), null, HttpStatus.CONFLICT);
        } catch (Exception e) {
            res.put("message", e.getMessage());
            res.put("error", e.getCause());
            res.put("data", new JSONObject());
            return new ResponseEntity<>(res.toString(), null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        res.put("message", "Successfully created the player");
        res.put("data", player.toJson());
        res.put("error", new JSONObject());
        return new ResponseEntity<>(res.toString(), null, HttpStatus.OK);
    }

    @PutMapping("/players/{playerId}")
    @Operation(summary = "Update a specific player", description = "Update the player with id matching parameter")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Successfully updated the player",
                    content = @Content(
                            mediaType = "application/json"
                    )
            ),
            @ApiResponse(responseCode = "400", description = "Wrong parameters",
                    content = @Content(
                            mediaType = "application/json"
                    )
            ),
            @ApiResponse(responseCode = "404", description = "No player found",
                    content = @Content(
                            mediaType = "application/json"
                    )
            ),
            @ApiResponse(responseCode = "409", description = "There is a player with the same name",
                    content = @Content(
                            mediaType = "application/json"
                    )
            ),
            @ApiResponse(responseCode = "500", description = "Internal server error",
                    content = @Content(
                            mediaType = "application/json"
                    )
            )
    })
    public ResponseEntity<String> updatePlayer(@PathVariable long playerId, @RequestBody PlayerRequest req) {
        JSONObject res = new JSONObject();
        JSONObject playerJSON = new JSONObject();
        Player player;

        try {
            if (gameService.playerWithSameName(req.getName())) throw new PlayerException.PlayerWithSameNameAlreadyExist(String.format("Player with name %s already exist", req.getName()));
            player = gameService.getPlayerByID(playerId);
            playerJSON.put("beforeUpdate", player.toJson());
            player.setNom(req.getName());
            playerJSON.put("afterUpdate", player.toJson());
        } catch (IllegalArgumentException e) {
            res.put("message", e.getMessage());
            res.put("data", new JSONObject());
            res.put("error", e.getCause());
            return new ResponseEntity<>(res.toString(), null, HttpStatus.BAD_REQUEST);
        } catch (PlayerException.PlayerNotExistException e) {
            res.put("message", e.getMessage());
            res.put("error", e.getCause());
            res.put("data", new JSONObject());
            return new ResponseEntity<>(res.toString(), null, HttpStatus.NOT_FOUND);
        } catch (PlayerException.PlayerWithSameNameAlreadyExist e) {
            res.put("message", e.getMessage());
            res.put("error", e.getCause());
            res.put("data", new JSONObject());
            return new ResponseEntity<>(res.toString(), null, HttpStatus.CONFLICT);
        } catch (Exception e) {
            res.put("message", e.getMessage());
            res.put("error", e.getCause());
            res.put("data", new JSONObject());
            return new ResponseEntity<>(res.toString(), null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        res.put("message", "Successfully updated the player");
        res.put("error", new JSONObject());
        res.put("data", playerJSON);
        return new ResponseEntity<>(res.toString(), null, HttpStatus.OK);
    }

    @DeleteMapping("/players/{playerId}")
    @Operation(summary = "Delete a specific player", description = "Delete the player with id matching parameter")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Successfully deleted the player",
                    content = @Content(
                            mediaType = "application/json"
                    )
            ),
            @ApiResponse(responseCode = "400", description = "Wrong parameters",
                    content = @Content(
                            mediaType = "application/json"
                    )
            ),
            @ApiResponse(responseCode = "404", description = "No player found",
                    content = @Content(
                            mediaType = "application/json"
                    )
            ),
            @ApiResponse(responseCode = "500", description = "Internal server error",
                    content = @Content(
                            mediaType = "application/json"
                    )
            )
    })
    public ResponseEntity<String> deletePlayer(@PathVariable long playerId) {
        JSONObject res = new JSONObject();
        Player player;
        try {
            player = gameService.getPlayerByID(playerId);
            gameService.removePlayer(player);
        } catch (IllegalArgumentException e) {
            res.put("message", e.getMessage());
            res.put("error", e.getCause());
            res.put("data", new JSONObject());
            return new ResponseEntity<>(res.toString(), null, HttpStatus.BAD_REQUEST);
        } catch (PlayerException.PlayerNotExistException e) {
            res.put("message", e.getMessage());
            res.put("data", new JSONObject());
            res.put("error", e.getCause());
            return new ResponseEntity<>(res.toString(), null, HttpStatus.NOT_FOUND);
        } catch (Exception e) {
            res.put("message", e.getMessage());
            res.put("data", new JSONObject());
            res.put("error", e.getCause());
            return new ResponseEntity<>(res.toString(), null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        res.put("message", "Player successfully deleted");
        res.put("data", player.toJson());
        res.put("error", new JSONObject());
        return new ResponseEntity<>(res.toString(), null, HttpStatus.OK);
    }

    @GetMapping("/database/save/")
    @Operation(summary = "Get list of saves", description = "Get list of saves for all databases")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "All saves successfully retrieved",
                    content = @Content(
                            mediaType = "application/json"
                    )
            ),
            @ApiResponse(responseCode = "500", description = "Internal server error",
                    content = @Content(
                            mediaType = "application/json"
                    )
            )
    })
    public ResponseEntity<String> getSaveList() {
        JSONObject res = new JSONObject();
        try {
            res.put("data", DatabaseFactory.listAllSaves());
            res.put("message", "Database successfully saved");
            res.put("error", new JSONObject());
        } catch (Exception e) {
            res.put("message", e.getMessage());
            res.put("data", new JSONObject());
            res.put("error", e.getCause());
            return new ResponseEntity<>(res.toString(), null, HttpStatus.INTERNAL_SERVER_ERROR);
        }

        return new ResponseEntity<>(res.toString(), null, HttpStatus.OK);
    }

    @PostMapping("/database/save/{database}")
    @Operation(summary = "Save database", description = "Save a specific database")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Database successfully saved",
                    content = @Content(
                            mediaType = "application/json"
                    )
            ),
            @ApiResponse(responseCode = "500", description = "Internal server error",
                    content = @Content(
                            mediaType = "application/json"
                    )
            )
    })
    public ResponseEntity<String> saveDatabase(@PathVariable String database) {
        JSONObject res = new JSONObject();
        try {
            DatabaseFactory.getInstance(Database.valueOf(database)).save();
        } catch (Exception e) {
            res.put("message", e.getMessage());
            res.put("data", new JSONObject());
            res.put("error", e.getCause());
            return new ResponseEntity<>(res.toString(), null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        res.put("message", "Database successfully saved");
        res.put("data", new JSONObject());
        res.put("error", new JSONObject());
        return new ResponseEntity<>(res.toString(), null, HttpStatus.OK);
    }

    @PostMapping("/database/swap/{database}")
    @Operation(summary = "Save database", description = "Save a specific database")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Database successfully saved",
                    content = @Content(
                            mediaType = "application/json"
                    )
            ),
            @ApiResponse(responseCode = "500", description = "Internal server error",
                    content = @Content(
                            mediaType = "application/json"
                    )
            )
    })
    public ResponseEntity<String> swapDatabase(@PathVariable String database) {
        JSONObject res = new JSONObject();
        try {
            DatabaseFactory.swapTo(Database.valueOf(database));
        } catch (Exception e) {
            res.put("message", e.getMessage());
            res.put("data", new JSONObject());
            res.put("error", e.getCause());
            return new ResponseEntity<>(res.toString(), null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        res.put("message", "Database successfully swaped");
        res.put("data", new JSONObject());
        res.put("error", new JSONObject());
        return new ResponseEntity<>(res.toString(), null, HttpStatus.OK);
    }

    @DeleteMapping("/database/{database}")
    @Operation(summary = "Dump database", description = "Dump a specific database")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Database successfully dumped",
                    content = @Content(
                            mediaType = "application/json"
                    )
            ),
            @ApiResponse(responseCode = "500", description = "Internal server error",
                    content = @Content(
                            mediaType = "application/json"
                    )
            )
    })
    public ResponseEntity<String> dumpDatabase(@PathVariable String database) {
        JSONObject res = new JSONObject();
        try {
            DatabaseFactory.getInstance(Database.valueOf(database)).dump();
        } catch (Exception e) {
            res.put("message", e.getMessage());
            res.put("data", new JSONObject());
            res.put("error", e.getCause());
            return new ResponseEntity<>(res.toString(), null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        res.put("message", "Database successfully dumped");
        res.put("data", new JSONObject());
        res.put("error", new JSONObject());
        return new ResponseEntity<>(res.toString(), null, HttpStatus.OK);
    }
}