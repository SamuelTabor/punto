package univ.ubs.punto.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import univ.ubs.punto.Config;
import univ.ubs.punto.database.DatabaseConnector;
import univ.ubs.punto.database.DatabaseFactory;
import univ.ubs.punto.database.sqlite.SQLiteConnector;
import univ.ubs.punto.exceptions.PlayerException;
import univ.ubs.punto.model.Game;
import univ.ubs.punto.model.Player;
import univ.ubs.punto.database.Database;

import java.util.ArrayList;
import java.util.Arrays;

import static univ.ubs.punto.database.Database.*;

@Service
public class GameService {
    private static final Logger logger = LoggerFactory.getLogger(GameService.class);
    private static GameService instance;
    private final ArrayList<Game> games;
    private final ArrayList<Player> players;
    private static long lastTimestamp = -1;

    //****************************************************************************************************************//
    //************************************************** CONSTRUCTORS ***********************************************//

    private GameService() {
        this.games = new ArrayList<>();
        this.players = new ArrayList<>();
        logger.debug("GameService created");
    }

    //****************************************************************************************************************//
    //************************************************** GETTERS ****************************************************//

    /**
     * Return the instance of the GameService
     * @return the instance of the GameService
     */
    public static GameService getInstance() {
        if (instance == null) {
            instance = new GameService();
            logger.debug("GameService instance created");
        }
        logger.debug("GameService instance returned.");
        return instance;
    }

    /**
     * Return the list of games
     * @return the list of games
     */
    public ArrayList<Game> getGames() {
        return this.games;
    }

    /**
     * Return the game with id matching parameter
     * @param id the id of the game
     * @return the game if the id match, null otherwise.
     * @throws IllegalArgumentException if id is negative
     */
    public Game getGameByID(long id) {
        if (id < 0) {
            throw new IllegalArgumentException("Game id cannot be negative");
        }

        for (Game game: this.games) {
            if (game.getGameId() == id) {
                return game;
            }
        }
        logger.error(String.format("Game with id %d not found", id));
        return null;
    }

    /**
     * Return the list of players
     * @return the list of players
     */
    public ArrayList<Player> getPlayers() {
        return this.players;
    }

    /**
     * Return the list of players
     * @return the list of players
     */
    public ArrayList<Player> getPlayers(ArrayList<Long> playersId) throws PlayerException.PlayerNotExistException {
        ArrayList<Player> players = new ArrayList<>();
        for (Long id: playersId) {
            try {
                players.add(getPlayerByID(id));
            } catch (PlayerException.PlayerNotExistException e) {
                throw new PlayerException.PlayerNotExistException(e.getMessage());
            }
        }
        return players;
    }

    /**
     * Return the player with id matching parameter
     * @param id the id of the player
     * @return the player if the id match
     * @throws IllegalArgumentException if id is negative
     */
    public Player getPlayerByID(long id) throws PlayerException.PlayerNotExistException {
        if (id < 0) {
            throw new IllegalArgumentException("Player id cannot be negative");
        }
        Player res = null;
        for (Player player: this.players) {
            if (player.getPlayerId() == id) {
                res = player;
            }
        }
        if (res != null) {
            return res;
        } else {
            logger.error(String.format("Player with id %d not found", id));
            throw new PlayerException.PlayerNotExistException(String.format("Player with id %d not found", id));
        }

    }


    //****************************************************************************************************************//
    //************************************************** METHODS ****************************************************//

    public boolean addGame(Game game) {
        if (game == null) {
            throw new IllegalArgumentException("Game cannot be null");
        }
        if (this.games.contains(game)) {
            logger.warn(String.format("Game %d already in the list", game.getGameId()));
            return false;
        }
        if (this.games.add(game)) {
            logger.debug(String.format("Game %d added to the list", game.getGameId()));
            /*try {
                DatabaseFactory.getInstance().insert(game);
            } catch (Exception e) {
                throw new RuntimeException(e);
            }*/
            return true;
        } else {
            logger.warn(String.format("Game %d not added to the list", game.getGameId()));
            return false;
        }
    }

    public boolean removeGame(Game game) {
        if (game == null) {
            throw new IllegalArgumentException("Game cannot be null");
        }
        if (this.games.remove(game)){
            logger.debug(String.format("Game %d removed from the list", game.getGameId()));
            return true;
        } else {
            logger.warn(String.format("Game %d not found in the list", game.getGameId()));
            return false;
        }
    }


    /**
     * Add a player to the list of players
     * @param player the player to add
     * @return true if the player is added, false otherwise
     * @throws IllegalArgumentException if player is null
     * @throws PlayerException.PlayerWithSameNameAlreadyExist if a player with the same name already exists
     */
    public boolean addPlayer(Player player) throws PlayerException.PlayerWithSameNameAlreadyExist {
        if (player == null) {
            throw new IllegalArgumentException("Player cannot be null");
        }
        if (this.players.contains(player)) {
            logger.warn(String.format("Player %d already in the list", player.getPlayerId()));
            return false;
        }
        if (playerWithSameName(player.getNom())) throw new PlayerException.PlayerWithSameNameAlreadyExist(String.format("Player with name %s already exist", player.getNom()));

        if (this.players.add(player)) {
            logger.debug(String.format("Player %d added to the list", player.getPlayerId()));
            try {
                DatabaseFactory.getInstance().insert(player);
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
            return true;
        } else {
            logger.warn(String.format("Player %d not added to the list", player.getPlayerId()));
            return false;
        }
    }

    /**
     * Check if a player with the same name already exists
     * @param name the name of the player
     * @return true if a player with the same name exists, false otherwise
     */
    public boolean playerWithSameName(String name) {
        for (Player player: this.players) {
            if (player.getNom().equalsIgnoreCase(name)) {
                return true;
            }
        }
        return false;
    }

    /**
     * Remove a player from the list of players
     * @param player the player to remove
     * @return true if the player is removed, false otherwise
     * @throws IllegalArgumentException if player is null
     */
    public boolean removePlayer(Player player) {
        if (player == null) {
            throw new IllegalArgumentException("Player cannot be null");
        }
        if (this.players.remove(player)){
            logger.debug(String.format("Player %d removed from the list", player.getPlayerId()));
            return true;
        } else {
            logger.warn(String.format("Player %d not found in the list", player.getPlayerId()));
            return false;
        }
    }

    public static synchronized long generateID() {
        long timestamp = System.currentTimeMillis();
        if (timestamp == lastTimestamp) {
            try {
                Thread.sleep(5);
            } catch (InterruptedException e) {
                logger.warn("Thread interrupted while trying to get new generated id based on system timestamp.");
                logger.debug(Arrays.toString(e.getStackTrace()));
                Thread.currentThread().interrupt();
            }
            timestamp = System.currentTimeMillis();
        }
        lastTimestamp = timestamp;
        return timestamp;
    }

    /**
     * Convert the list of games to a json string
      * @return the list of games as a json string
     */
    public String gamesToJson() {
        StringBuilder res = new StringBuilder("[");
        for (int i = 0; i < this.games.size(); i++) {
            res.append(this.games.get(i).toJson());
            if (i < this.games.size() - 1) {
                res.append(",");
            }
        }
        res.append("]");
        return res.toString();
    }
}