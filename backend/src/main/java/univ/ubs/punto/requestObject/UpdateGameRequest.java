package univ.ubs.punto.requestObject;

import univ.ubs.punto.model.Player;

import java.util.List;

public class UpdateGameRequest {
    private int nbRounds;
    private List<Player> players;
    private String description;

    public int getNbRounds() {
        return nbRounds;
    }

    public void setNbRounds(int nbRounds) {
        this.nbRounds = nbRounds;
    }

    public List<Player> getPlayers() {
        return players;
    }

    public void setPlayers(List<Player> players) {
        this.players = players;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
