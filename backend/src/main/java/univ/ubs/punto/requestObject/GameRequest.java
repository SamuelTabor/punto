package univ.ubs.punto.requestObject;

import org.springframework.lang.Nullable;

import java.util.List;

public class GameRequest {
    @Nullable
    private int nbRounds;
    @Nullable
    private List<Long> players;
    @Nullable
    private String description;

    public int getNbRounds() {
        return nbRounds;
    }

    public void setNbRounds(int nbRounds) {
        this.nbRounds = nbRounds;
    }

    public List<Long> getPlayers() {
        return players;
    }

    public void setPlayers(List<Long> players) {
        this.players = players;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}