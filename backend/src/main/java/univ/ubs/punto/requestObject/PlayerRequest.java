package univ.ubs.punto.requestObject;

public class PlayerRequest {
    private String name;

    public PlayerRequest() {
    }

    public PlayerRequest(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
