package univ.ubs.punto.exceptions;

import univ.ubs.punto.model.Player;

public class PlayerException extends Exception {
    public PlayerException(String message) {
        super(message);
    }

    public PlayerException(String message, Throwable cause) {
        super(message, cause);
    }

    public PlayerException(Throwable cause) {
        super(cause);
    }

    public static class PlayerWithSameNameAlreadyExist extends PlayerException {
        public PlayerWithSameNameAlreadyExist(String message) {
            super(message);
        }
    }

    public static class PlayerNotExistException extends PlayerException {
        public PlayerNotExistException(String message) {
            super(message);
        }
    }

}
