package univ.ubs.punto.exceptions;

public class RoundException extends Exception {
    public RoundException(String message) {
        super(message);
    }

    public RoundException(String message, Throwable cause) {
        super(message, cause);
    }

    public RoundException(Throwable cause) {
        super(cause);
    }

    public static class EmplyPlayerCardsHandException extends RoundException {
        public EmplyPlayerCardsHandException(String message) {
            super(message);
        }
    }

}
