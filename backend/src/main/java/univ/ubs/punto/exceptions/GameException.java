package univ.ubs.punto.exceptions;

import com.fasterxml.jackson.databind.DatabindException;
import univ.ubs.punto.requestObject.GameRequest;

public class GameException extends Exception {
    // Constructeurs
    public GameException() {
        super();
    }

    public GameException(String message) {
        super(message);
    }

    public GameException(String message, Throwable cause) {
        super(message, cause);
    }

    public static class GameNotExistException extends GameException {
        public GameNotExistException(String gameNotFound) {
            super(gameNotFound);
        }
    }

    public static class MoveNotAllowedException extends GameException {
        public MoveNotAllowedException(String moveNotAllowed) {
            super(moveNotAllowed);
        }
    }
}

