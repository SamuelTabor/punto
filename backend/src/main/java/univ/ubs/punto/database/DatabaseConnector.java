package univ.ubs.punto.database;

import univ.ubs.punto.database.mysql.MySqlException;
import univ.ubs.punto.dto.MoveDTO;
import univ.ubs.punto.model.Board;
import univ.ubs.punto.model.Game;
import univ.ubs.punto.model.Player;
import univ.ubs.punto.model.Round;

public interface DatabaseConnector {

    //****************************************************************************************************************//
    //**********************************************  CRUD for Game  *************************************************//
    void insert(Game game) throws Exception;

    void update(Game game) throws Exception;

    void delete(Game game) throws Exception;

    Game select(long id) throws Exception;

    //****************************************************************************************************************//
    //**********************************************  CRUD for Round  ************************************************//

    void insert(Round round) throws Exception;

    void update(Round round) throws Exception;

    void delete(Round round) throws Exception;

    Round select(int id) throws Exception;

    //****************************************************************************************************************//
    //**********************************************  CRUD for Player  ***********************************************//

    void insert(Player player) throws Exception;

    void update(Player player) throws Exception;

    void delete(Player player) throws Exception;

    Player select(String id) throws Exception;

    //****************************************************************************************************************//
    //*********************************************  Insert for Move  ************************************************//

    void insert(long gameId, int roundId, long playerId, int row, int column, String cardColor, int cardNumber, boolean isAllowed) throws Exception;

    //****************************************************************************************************************//
    //**********************************************  Dump and Extract  **********************************************//

    void dump() throws Exception;


    void save();

    void load(String path) throws Exception;

    void generateCSV();
}
