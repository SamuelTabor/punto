package univ.ubs.punto.database;

public enum Database {
    MYSQL,
    MONGODB,
    SQLITE,
    NEO4J;
}
