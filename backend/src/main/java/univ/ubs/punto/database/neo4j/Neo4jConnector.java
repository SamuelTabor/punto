package univ.ubs.punto.database.neo4j;

import com.opencsv.CSVReader;
import com.opencsv.CSVWriter;
import com.opencsv.exceptions.CsvException;
import org.neo4j.driver.*;
import org.neo4j.driver.Record;
import org.slf4j.LoggerFactory;
import univ.ubs.punto.Config;
import univ.ubs.punto.database.DatabaseConnector;
import univ.ubs.punto.model.Game;
import univ.ubs.punto.model.Player;
import univ.ubs.punto.model.Round;

import java.io.*;
import java.sql.Date;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

import static org.neo4j.driver.Values.parameters;

public class Neo4jConnector implements DatabaseConnector {
    private static Neo4jConnector instance;
    private static Driver driver;
    private static final String DATABASE_URL = "bolt://localhost:7687"; //Si marche pas, tester 127.0.0.1
    private static final String USERNAME = "neo4j";
    private static final String PASSWORD = "neo4jAdmin";
    public static final String BACKUP_PATH = Config.WORKING_DIRECTORY + "/databases/Neo4j/save/";
    private static final String CSV_PATH = Config.WORKING_DIRECTORY + "/databases/csv/";

    private Neo4jConnector() {
    }

    public static Neo4jConnector getInstance() {
        if (instance == null) {
            instance = new Neo4jConnector();
        }
        return instance;
    }


    public static Driver getConnection() {
        if (driver == null) {
            driver = GraphDatabase.driver(DATABASE_URL, AuthTokens.basic(USERNAME, PASSWORD));
        }
        return driver;
    }

    //****************************************************************************************************************//
    //**********************************************  CRUD for Player  ***********************************************//

    public void insert(Player player) {
        try (var session = getConnection().session()) {
            session.writeTransaction(tx -> {
                // Check if the player already exists
                String checkQuery = "MATCH (p:Player {playerId: $playerId}) RETURN p";
                Result checkResult = tx.run(checkQuery, parameters("playerId", player.getPlayerId()));
                if (!checkResult.hasNext()) {
                    String query = "CREATE (p:Player {playerId: $playerId, nom: $nom, gameScore: $gameScore, roundScore: $roundScore, created: timestamp(), last_updated: timestamp()})";
                    tx.run(query, parameters("playerId", player.getPlayerId(), "nom", player.getNom(), "gameScore", player.getGameScore(), "roundScore", player.getRoundScore()));
                } else {
                    LoggerFactory.getLogger(Neo4jConnector.class).error("Player with id " + player.getPlayerId() + " already exists");
                }
                return null;
            });
        }
    }

    public void update(Player player) {
        try (var session = getConnection().session()) {
            session.writeTransaction(tx -> {
                String query = "MATCH (p:Player {playerId: $playerId}) SET p.nom = $nom, p.gameScore = $gameScore, p.roundScore = $roundScore, p.last_updated = timestamp()";
                tx.run(query, parameters("playerId", player.getPlayerId(), "nom", player.getNom(), "gameScore", player.getGameScore(), "roundScore", player.getRoundScore()));
                return null;
            });
        }
    }

    public void delete(Player player) {
        try (var session = getConnection().session()) {
            session.writeTransaction(tx -> {
                String query = "MATCH (p:Player {playerId: $playerId}) DELETE p";
                tx.run(query, parameters("playerId", player.getPlayerId()));
                return null;
            });
        }
    }

    public Player select(String id) {
        return null;
    }

    //****************************************************************************************************************//
    //**********************************************  CRUD for Game  *************************************************//

    public void insert(Game game) {
        // Insert the game in db
        try (var session = getConnection().session()) {
            String query = "CREATE (g:Game {gameId: $gameId, gameDescription: $gameDescription, nbRounds: $nbRounds, gameStatus: $gameStatus, currentRound: $currentRound, flagModifiable: $flagModifiable, created: timestamp(), last_updated: timestamp()})";
            executeQuery(session, query, parameters("gameId", game.getGameId(), "gameDescription", game.getGameDescription(), "nbRounds", game.getNbRounds(), "gameStatus", game.getGameStatus().toString(), "currentRound", game.getCurrentRound().getRoundId(), "flagModifiable", game.getFlagModifiable()));


            // Insert the players and add a link with the game
            for (Player player : game.getPlayers()) {
                insert(player);
                linkPlayerToGame(session, game.getGameId(), player.getPlayerId());
            }

            // Insert the rounds and add a link with the game
            for (Round round : game.getRounds()) {
                insert(round);
                linkRoundToGame(session, game.getGameId(), round.getRoundId());
            }

            // Insert the winners and add a link with the game
            if (game.getGameWinners() != null) {
                for (Player player : game.getGameWinners()) {
                    linkWinnerToGame(session, game.getGameId(), player.getPlayerId());
                }
            }
        }
    }

    public void update(Game game) {
        try (var session = getConnection().session()) {
            String querry = "MATCH (g:Game {gameId: $gameId}) SET g.gameDescription = $gameDescription, g.nbRounds = $nbRounds, g.gameStatus = $gameStatus, g.currentRound = $currentRound, g.last_updated = timestamp()";
            executeQuery(session, querry, parameters("gameId", game.getGameId(), "gameDescription", game.getGameDescription(), "nbRounds", game.getNbRounds(), "gameStatus", game.getGameStatus().toString(), "currentRound", game.getCurrentRound().getRoundId()));

            // Update the players and add a link with the game
            for (Player player : game.getPlayers()) {
                update(player);
            }

            // Update the rounds and add a link with the game
            for (Round round : game.getRounds()) {
                update(round);
            }

            // Update the winners and add a link with the game
            if (game.getGameWinners() != null) {
                for (Player player : game.getGameWinners()) {
                    linkWinnerToGame(session, game.getGameId(), player.getPlayerId());
                }
            }
        }
    }

    public void delete(Game game) {
        try (var session = getConnection().session()) {
            session.writeTransaction(tx -> {
                String query = "MATCH (g:Game {gameId: $gameId}) DELETE g";
                tx.run(query, parameters("gameId", game.getGameId()));
                return null;
            });
        }
    }

    public Game select(long id) {
        return null;
    }

    //****************************************************************************************************************//
    //**********************************************  CRUD for Round  ************************************************//

    public void insert(Round round) {
        try (var session = getConnection().session()) {
            String query = "CREATE (r:Round {roundId: $roundId, gameId: $gameId, roundStatus: $roundStatus, currentRoundNUm: $currentRoundNUm, flagModifiable: $flagModifiable, created: timestamp(), last_updated: timestamp()})";
            executeQuery(session, query, parameters("roundId", round.getRoundId(), "gameId", round.getGameId(), "roundStatus", round.getRoundStatus().toString(), "currentRoundNUm", round.getRoundId(), "flagModifiable", round.getFlagModifiable()));

            // Insert the winners and add a link with the round
            if (round.getWinners() != null) {
                for (Player player : round.getWinners()) {
                    linkWinnerToRound(session, round.getGameId(), round.getRoundId(), player.getPlayerId());
                }
            }
        }

    }

    public void update(Round round) {
        try (var session = getConnection().session()) {
            String query = "MATCH (r:Round {roundId: $roundId, gameId: $gameId}) SET r.roundStatus = $roundStatus, r.currentRoundNUm = $currentRoundNUm, r.last_updated = timestamp()";
            executeQuery(session, query, parameters("roundId", round.getRoundId(), "gameId", round.getGameId(), "roundStatus", round.getRoundStatus().toString(), "currentRoundNUm", round.getRoundId()));


            // Update the winners and add a link with the round
            if (round.getWinners() != null) {
                for (Player player : round.getWinners()) {
                    linkWinnerToRound(session, round.getGameId(), round.getRoundId(), player.getPlayerId());
                }
            }
        }
    }

    public void delete(Round round) {
        try (var session = getConnection().session()) {
            session.writeTransaction(tx -> {
                String query = "MATCH (r:Round {roundId: $roundId, gameId: $gameId}) DELETE r";
                tx.run(query, parameters("roundId", round.getRoundId(), "gameId", round.getGameId()));
                return null;
            });
        }
    }

    public Round select(int id) {
        return null;
    }

    //****************************************************************************************************************//
    //*********************************************  Insert for Move  ************************************************//

    public void insert(long gameId, int roundId, long playerId, int row, int column, String cardColor, int cardNumber, boolean isAllowed) {
        try (var session = getConnection().session()) {
            // Create Move node
            String moveQuery = "CREATE (m:Move {row: $row, column: $column, cardColor: $cardColor, cardNumber: $cardNumber, isAllowed: $isAllowed, created: timestamp(), last_updated: timestamp()}) RETURN id(m) as id";
            Result moveResult = session.run(moveQuery, parameters("row", row, "column", column, "cardColor", cardColor, "cardNumber", cardNumber, "isAllowed", isAllowed));
            if (!moveResult.hasNext()) {
                throw new RuntimeException("Failed to create Move node");
            }
            int moveId = moveResult.next().get("id").asInt();

            // Link Move to Player
            String playerQuery = "MATCH (p:Player {playerId: $playerId}), (m:Move) WHERE id(m) = $moveId CREATE (p)-[:PLAYS]->(m)";
            session.run(playerQuery, parameters("playerId", playerId, "moveId", moveId));

            // Link Move to Round
            String roundQuery = "MATCH (r:Round {roundId: $roundId, gameId: $gameId}), (m:Move) WHERE id(m) = $moveId CREATE (m)-[:MOVE_IN]->(r)";
            session.run(roundQuery, parameters("roundId", roundId, "gameId", gameId, "moveId", moveId));
        }
    }

    //****************************************************************************************************************//
    //**********************************************  Dump and Extract  **********************************************//

    public void dump() {
        try (var session = getConnection().session()) {
            session.writeTransaction(tx -> {
                String query = "MATCH (n) DETACH DELETE n";
                tx.run(query);
                return null;
            });
        }
    }


    public void save() {
        // Obtenez la date actuelle pour créer un dossier de sauvegarde unique
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd_HH-mm-ss");
        String backupDate = dateFormat.format(new Date(System.currentTimeMillis()));

        // Créez le nom du dossier avec le préfixe "backup_date_mysql"
        String backupFolderName = "backup_" + backupDate + "_neo4j/";
        String backupFolderPath = BACKUP_PATH + backupFolderName;

        exportPlayersToCSV(backupFolderPath);
        exportGamesToCSV(backupFolderPath);
        exportRoundsToCSV(backupFolderPath);
        exportMovesToCSV(backupFolderPath);
        exportRoundWinnersToCSV(backupFolderPath);
        exportGameWinnersToCSV(backupFolderPath);
        exportPlayersGamesToCSV(backupFolderPath);
    }

    public void load(String path) {
        try (var session = getConnection().session()) {
            importPlayersFromCSV(session, path);
            importGamesFromCSV(session, path);
            importRoundsFromCSV(session, path);
            importMovesFromCSV(session, path);
            importRoundWinnersFromCSV(session, path);
            importGameWinnersFromCSV(session, path);
            importPlayersGamesFromCSV(session, path);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void generateCSV() {
        exportPlayersToCSV(CSV_PATH);
        exportGamesToCSV(CSV_PATH);
        exportRoundsToCSV(CSV_PATH);
        exportMovesToCSV(CSV_PATH);
        exportRoundWinnersToCSV(CSV_PATH);
        exportGameWinnersToCSV(CSV_PATH);
        exportPlayersGamesToCSV(CSV_PATH);
    }

    private void executeQuery(Session session, String query, Value parameters) {
        try (var tx = session.beginTransaction()) {
            tx.run(query, parameters);
            tx.commit();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    //****************************************************************************************************************//
    //****************************************************  Links  ***************************************************//

    public void linkPlayerToGame(Session session, long gameId, long playerId) {
        try {
            String playerQuery = "MATCH (p:Player {playerId: $playerId}) RETURN p";
            Result playerResult = session.run(playerQuery, parameters("playerId", playerId));
            if (!playerResult.hasNext()) {
                throw new RuntimeException("Player with id " + playerId + " does not exist");
            }

            String gameQuery = "MATCH (g:Game {gameId: $gameId}) RETURN g";
            Result gameResult = session.run(gameQuery, parameters("gameId", gameId));
            if (!gameResult.hasNext()) {
                throw new RuntimeException("Game with id " + gameId + " does not exist");
            }

            String relationQuery = "MATCH (p:Player {playerId: $playerId}), (g:Game {gameId: $gameId}) CREATE (p)-[:PLAYS_IN]->(g)";
            executeQuery(session, relationQuery, parameters("playerId", playerId, "gameId", gameId));

        } catch (RuntimeException e) {
            throw new RuntimeException(e);
        }
    }

    public void linkRoundToGame(Session session, long gameId, int roundId) {
        try {
            String gameQuery = "MATCH (g:Game {gameId: $gameId}) RETURN g";
            Result gameResult = session.run(gameQuery, parameters("gameId", gameId));
            if (!gameResult.hasNext()) {
                throw new RuntimeException("Game with id " + gameId + " does not exist");
            }

            String roundQuery = "MATCH (r:Round {roundId: $roundId, gameId: $gameId}) RETURN r";
            Result roundResult = session.run(roundQuery, parameters("roundId", roundId, "gameId", gameId));
            if (!roundResult.hasNext()) {
                throw new RuntimeException("Round with id " + roundId + " does not exist in game " + gameId);
            }


            String relationQuery = "MATCH (r:Round {roundId: $roundId, gameId: $gameId}), (g:Game {gameId: $gameId}) CREATE (r)-[:ROUND_IN]->(g)";
            executeQuery(session, relationQuery, parameters("roundId", roundId, "gameId", gameId));
        } catch (RuntimeException e) {
            throw new RuntimeException(e);
        }
    }

    public void linkWinnerToGame(Session session, long gameId, long playerId) {
        try {

            String playerQuery = "MATCH (p:Player {playerId: $playerId}) RETURN p";
            Result playerResult = session.run(playerQuery, parameters("playerId", playerId));
            if (!playerResult.hasNext()) {
                throw new RuntimeException("Player with id " + playerId + " does not exist");
            }

            String gameQuery = "MATCH (g:Game {gameId: $gameId}) RETURN g";
            Result gameResult = session.run(gameQuery, parameters("gameId", gameId));
            if (!gameResult.hasNext()) {
                throw new RuntimeException("Game with id " + gameId + " does not exist");
            }

            String relationQuery = "MATCH (p:Player {playerId: $playerId}), (g:Game {gameId: $gameId}) CREATE (p)-[:WON_GAME]->(g)";
            executeQuery(session, relationQuery, parameters("playerId", playerId, "gameId", gameId));

        } catch (RuntimeException e) {
            throw new RuntimeException(e);
        }
    }

    public void linkWinnerToRound(Session session, long gameId, int roundId, long playerId) {
        try {
            String checkQuery = "MATCH (p:Player {playerId: $playerId})-[:WON_ROUND]->(r:Round {roundId: $roundId, gameId: $gameId}) RETURN p";
            Result checkResult = session.run(checkQuery, parameters("playerId", playerId, "roundId", roundId, "gameId", gameId));
            if (!checkResult.hasNext()) {
                String playerQuery = "MATCH (p:Player {playerId: $playerId}) RETURN p";
                Result playerResult = session.run(playerQuery, parameters("playerId", playerId));
                if (!playerResult.hasNext()) {
                    throw new RuntimeException("Player with id " + playerId + " does not exist");
                }

                String gameQuery = "MATCH (g:Game {gameId: $gameId}) RETURN g";
                Result gameResult = session.run(gameQuery, parameters("gameId", gameId));
                if (!gameResult.hasNext()) {
                    throw new RuntimeException("Game with id " + gameId + " does not exist");
                }

                String roundQuery = "MATCH (r:Round {roundId: $roundId, gameId: $gameId}) RETURN r";
                Result roundResult = session.run(roundQuery, parameters("roundId", roundId, "gameId", gameId));
                if (!roundResult.hasNext()) {
                    throw new RuntimeException("Round with id " + roundId + " does not exist in round " + gameId);
                }


                String relationQuery = "MATCH (p:Player {playerId: $playerId}), (r:Round {roundId: $roundId, gameId: $gameId}) CREATE (p)-[:WON_ROUND]->(r)";
                executeQuery(session, relationQuery, parameters("playerId", playerId, "roundId", roundId, "gameId", gameId));
            }
        } catch (RuntimeException e) {
            throw new RuntimeException(e);
        }
    }

    //****************************************************************************************************************//
    //**********************************************  CSV Extractors *************************************************//

    private void exportPlayersToCSV(String path) {
        List<String[]> data = new ArrayList<>();
        try (var session = getConnection().session()) {
            String query = "MATCH (p:Player) RETURN p";
            Result result = session.run(query);
            data.add(new String[]{"id", "nom", "gameScore", "roundScore", "created", "last_updated"});
            while (result.hasNext()) {
                Record record = result.next();
                Value playerValue = record.get("p");
                String playerId = String.valueOf(playerValue.get("playerId"));
                String nom = playerValue.get("nom").asString();
                String gameScore = String.valueOf(playerValue.get("gameScore"));
                String roundScore = String.valueOf(playerValue.get("roundScore"));
                String created = convertTimestampToSqlDate(String.valueOf(playerValue.get("created")));
                String last_updated = convertTimestampToSqlDate(String.valueOf(playerValue.get("last_updated")));

                data.add(new String[]{playerId, nom, gameScore, roundScore, created, last_updated});
            }
        }

        // Check if the directory exists, if not, create it
        File directory = new File(path);
        if (!directory.exists()) {
            directory.mkdirs();
        }

        // Check if the file exists, if not, create it
        File file = new File(path + "Players.csv");
        if (!file.exists()) {
            try {
                file.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        try (CSVWriter writer = new CSVWriter(new FileWriter(file), CSVWriter.DEFAULT_SEPARATOR, CSVWriter.NO_QUOTE_CHARACTER, CSVWriter.DEFAULT_ESCAPE_CHARACTER, CSVWriter.DEFAULT_LINE_END)) {
            writer.writeAll(data);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void exportGamesToCSV(String path) {
        List<String[]> data = new ArrayList<>();
        try (var session = getConnection().session()) {
            String query = "MATCH (g:Game) RETURN g";
            Result result = session.run(query);
            data.add(new String[]{"id", "description", "nbRounds", "gameStatus", "currentRound", "flagModifiable", "created", "last_updated"});
            while (result.hasNext()) {
                Record record = result.next();
                Value gameValue = record.get("g");
                String id = String.valueOf(gameValue.get("gameId"));
                String description = gameValue.get("description").isNull() ? "" : gameValue.get("description").asString();
                String nbRounds = String.valueOf(gameValue.get("nbRounds"));
                String gameStatus = gameValue.get("gameStatus").asString();
                String currentRound = String.valueOf(gameValue.get("currentRound"));
                String flagModifiable = String.valueOf(gameValue.get("flagModifiable").asBoolean() ? 1 : 0);
                String created = convertTimestampToSqlDate(String.valueOf(gameValue.get("created")));
                String last_updated = convertTimestampToSqlDate(String.valueOf(gameValue.get("last_updated")));

                data.add(new String[]{id, description, nbRounds, gameStatus, currentRound, flagModifiable, created, last_updated});
            }
        }

        // Check if the directory exists, if not, create it
        File directory = new File(path);
        if (!directory.exists()) {
            directory.mkdirs();
        }

        // Check if the file exists, if not, create it
        File file = new File(path + "Games.csv");
        if (!file.exists()) {
            try {
                file.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        try (CSVWriter writer = new CSVWriter(new FileWriter(file), CSVWriter.DEFAULT_SEPARATOR, CSVWriter.NO_QUOTE_CHARACTER, CSVWriter.DEFAULT_ESCAPE_CHARACTER, CSVWriter.DEFAULT_LINE_END)) {
            writer.writeAll(data);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void exportRoundsToCSV(String path) {
        List<String[]> data = new ArrayList<>();
        try (var session = getConnection().session()) {
            String query = "MATCH (r:Round) RETURN r";
            Result result = session.run(query);
            data.add(new String[]{"id", "gameId", "roundStatus", "currentRoundNum", "flagModifiable", "created", "last_updated"});
            while (result.hasNext()) {
                Record record = result.next();
                Value roundValue = record.get("r");

                String roundId = String.valueOf(roundValue.get("roundId"));
                String gameId = String.valueOf(roundValue.get("gameId"));
                String roundStatus = roundValue.get("roundStatus").asString();
                String currentRoundNUm = String.valueOf(roundValue.get("currentRoundNUm"));
                String flagModifiable = String.valueOf(roundValue.get("flagModifiable").asBoolean() ? 1 : 0);
                String created = convertTimestampToSqlDate(String.valueOf(roundValue.get("created")));
                String last_updated = convertTimestampToSqlDate(String.valueOf(roundValue.get("last_updated")));

                data.add(new String[]{roundId, gameId, roundStatus, currentRoundNUm, flagModifiable, created, last_updated});
            }
        }

        // Check if the file exists, if not, create it
        File file = new File(path + "Rounds.csv");
        if (!file.exists()) {
            try {
                file.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        try (CSVWriter writer = new CSVWriter(new FileWriter(file), CSVWriter.DEFAULT_SEPARATOR, CSVWriter.NO_QUOTE_CHARACTER, CSVWriter.DEFAULT_ESCAPE_CHARACTER, CSVWriter.DEFAULT_LINE_END)) {
            writer.writeAll(data);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    public void exportMovesToCSV(String path) {
        List<String[]> data = new ArrayList<>();
        try (var session = getConnection().session()) {
            String query = "MATCH (p:Player)-[pl:PLAYS]->(m:Move)-[mi:MOVE_IN]->(r:Round) RETURN id(m) as id, p, m, r";
            Result result = session.run(query);
            data.add(new String[]{"id", "gameId", "roundId", "playerId", "rowIndex", "colIndex", "cardNumber", "cardColor", "allowed", "created", "last_updated"});
            while (result.hasNext()) {
                Record record = result.next();
                Value playerValue = record.get("p");
                Value moveValue = record.get("m");
                Value roundValue = record.get("r");

                String id = String.valueOf(record.get("id").asInt());
                String gameId = String.valueOf(roundValue.get("gameId"));
                String roundId = String.valueOf(roundValue.get("roundId"));
                String playerId = String.valueOf(playerValue.get("playerId"));
                String row = String.valueOf(moveValue.get("row"));
                String column = String.valueOf(moveValue.get("column"));
                String cardColor = moveValue.get("cardColor").asString();
                String cardNumber = String.valueOf(moveValue.get("cardNumber"));
                String isAllowed = String.valueOf(moveValue.get("isAllowed").asBoolean() ? 1 : 0);

                String created = convertTimestampToSqlDate(String.valueOf(moveValue.get("created")));
                String last_updated = convertTimestampToSqlDate(String.valueOf(moveValue.get("last_updated")));

                data.add(new String[]{id, gameId, roundId, playerId, row, column, cardNumber, cardColor, isAllowed, created, last_updated});
            }
        }

        // Check if the file exists, if not, create it
        File file = new File(path + "Moves.csv");
        if (!file.exists()) {
            try {
                file.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        try (CSVWriter writer = new CSVWriter(new FileWriter(file), CSVWriter.DEFAULT_SEPARATOR, CSVWriter.NO_QUOTE_CHARACTER, CSVWriter.DEFAULT_ESCAPE_CHARACTER, CSVWriter.DEFAULT_LINE_END)) {
            writer.writeAll(data);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void exportRoundWinnersToCSV(String path) {
        List<String[]> data = new ArrayList<>();
        try (var session = getConnection().session()) {
            String query = "MATCH (p:Player)-[:WON_ROUND]->(r:Round)-[:ROUND_IN]->(g:Game) RETURN p, r, g";
            Result result = session.run(query);
            data.add(new String[]{"gameId", "roundId", "playerId"});

            while (result.hasNext()) {
                Record record = result.next();
                Value playerValue = record.get("p");
                Value roundValue = record.get("r");
                Value gameValue = record.get("g");

                String playerId = String.valueOf(playerValue.get("playerId"));
                String roundId = String.valueOf(roundValue.get("roundId"));
                String gameId = String.valueOf(gameValue.get("gameId"));

                data.add(new String[]{gameId, roundId, playerId});
            }
        }

        // Check if the file exists, if not, create it
        File file = new File(path + "RoundWinners.csv");
        if (!file.exists()) {
            try {
                file.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        try (CSVWriter writer = new CSVWriter(new FileWriter(file), CSVWriter.DEFAULT_SEPARATOR, CSVWriter.NO_QUOTE_CHARACTER, CSVWriter.DEFAULT_ESCAPE_CHARACTER, CSVWriter.DEFAULT_LINE_END)) {
            writer.writeAll(data);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void exportGameWinnersToCSV(String path) {
        List<String[]> data = new ArrayList<>();
        try (var session = getConnection().session()) {
            String query = "MATCH (p:Player)-[:WON_GAME]->(g:Game) RETURN p, g";
            Result result = session.run(query);
            data.add(new String[]{"gameId", "playerId"});
            while (result.hasNext()) {
                Record record = result.next();
                Value playerValue = record.get("p");
                Value gameValue = record.get("g");

                String playerId = String.valueOf(playerValue.get("playerId"));
                String gameId = String.valueOf(gameValue.get("gameId"));

                data.add(new String[]{gameId, playerId});
            }
        }

        // Check if the file exists, if not, create it
        File file = new File(path + "GameWinners.csv");
        if (!file.exists()) {
            try {
                file.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        try (CSVWriter writer = new CSVWriter(new FileWriter(file), CSVWriter.DEFAULT_SEPARATOR, CSVWriter.NO_QUOTE_CHARACTER, CSVWriter.DEFAULT_ESCAPE_CHARACTER, CSVWriter.DEFAULT_LINE_END)) {
            writer.writeAll(data);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void exportPlayersGamesToCSV(String path) {
        List<String[]> data = new ArrayList<>();
        try (var session = getConnection().session()) {
            String query = "MATCH (p:Player)-[:PLAYS_IN]->(g:Game) RETURN p, g";
            Result result = session.run(query);
            data.add(new String[]{"gameId", "playerId"});
            while (result.hasNext()) {
                Record record = result.next();
                Value playerValue = record.get("p");
                Value gameValue = record.get("g");

                String playerId = String.valueOf(playerValue.get("playerId"));
                String gameId = String.valueOf(gameValue.get("gameId"));

                data.add(new String[]{gameId, playerId});
            }
        }

        // Check if the file exists, if not, create it
        File file = new File(path + "PlayersGames.csv");
        if (!file.exists()) {
            try {
                file.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        try (CSVWriter writer = new CSVWriter(new FileWriter(file), CSVWriter.DEFAULT_SEPARATOR, CSVWriter.NO_QUOTE_CHARACTER, CSVWriter.DEFAULT_ESCAPE_CHARACTER, CSVWriter.DEFAULT_LINE_END)) {
            writer.writeAll(data);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    //****************************************************************************************************************//
    //**********************************************  CSV Loaders ****************************************************//

    private void importPlayersFromCSV(Session session, String path) {
        try (CSVReader reader = new CSVReader(new FileReader(path + "Players.csv"))) {
            List<String[]> lines = reader.readAll();
            // Skip the header row
            for (int i = 1; i < lines.size(); i++) {
                String[] line = lines.get(i);
                long playerId = Long.parseLong(line[0]);
                String nom = line[1];
                int gameScore = Integer.parseInt(line[2]);
                int roundScore = Integer.parseInt(line[3]);
                String created = line[4];
                String last_updated = line[5];


                String query = "CREATE (p:Player {playerId: $playerId, nom: $nom, gameScore: $gameScore, roundScore: $roundScore, created: apoc.date.parse($created, 'ms', 'yyyy-MM-dd HH:mm:ss'), last_updated: apoc.date.parse($last_updated, 'ms', 'yyyy-MM-dd HH:mm:ss')})";
                executeQuery(session, query, parameters("playerId", playerId, "nom", nom, "gameScore", gameScore, "roundScore", roundScore, "created", created, "last_updated", last_updated));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void importGamesFromCSV(Session session, String path) {
        try (CSVReader reader = new CSVReader(new FileReader(path + "Games.csv"))) {
            List<String[]> lines = reader.readAll();
            // Skip the header row
            for (int i = 1; i < lines.size(); i++) {
                String[] line = lines.get(i);
                long gameId = Long.parseLong(line[0]);
                String gameDescription = line[1];
                int nbRounds = Integer.parseInt(line[2]);
                String gameStatus = line[3];
                int currentRound = Integer.parseInt(line[4]);
                boolean flagModifiable = line[5].equals("1");
                String created = line[6];
                String last_updated = line[7];

                String query = "CREATE (g:Game {gameId: $gameId, gameDescription: $gameDescription, nbRounds: $nbRounds, gameStatus: $gameStatus, currentRound: $currentRound, flagModifiable: $flagModifiable, created: apoc.date.parse($created, 'ms', 'yyyy-MM-dd HH:mm:ss'), last_updated: apoc.date.parse($last_updated, 'ms', 'yyyy-MM-dd HH:mm:ss')})";
                executeQuery(session, query, parameters("gameId", gameId, "gameDescription", gameDescription, "nbRounds", nbRounds, "gameStatus", gameStatus, "currentRound", currentRound, "flagModifiable", flagModifiable, "created", created, "last_updated", last_updated));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void importRoundsFromCSV(Session session, String path) {
        try (CSVReader reader = new CSVReader(new FileReader(path + "Rounds.csv"))) {
            List<String[]> lines = reader.readAll();
            // Skip the header row
            for (int i = 1; i < lines.size(); i++) {
                String[] line = lines.get(i);
                int roundId = Integer.parseInt(line[0]);
                long gameId = Long.parseLong(line[1]);
                String roundStatus = line[2];
                int currentRoundNum = Integer.parseInt(line[3]);
                boolean flagModifiable = line[4].equals("1");
                String created = line[5];
                String last_updated = line[6];

                String query = "CREATE (r:Round {roundId: $roundId, gameId: $gameId, roundStatus: $roundStatus, currentRoundNUm: $currentRoundNum, flagModifiable: $flagModifiable, created: apoc.date.parse($created, 'ms', 'yyyy-MM-dd HH:mm:ss'), last_updated: apoc.date.parse($last_updated, 'ms', 'yyyy-MM-dd HH:mm:ss')})";
                executeQuery(session, query, parameters("roundId", roundId, "gameId", gameId, "roundStatus", roundStatus, "currentRoundNum", currentRoundNum, "flagModifiable", flagModifiable, "created", created, "last_updated", last_updated));

                String relationQuery = "MATCH (r:Round {roundId: $roundId, gameId: $gameId}), (g:Game {gameId: $gameId}) CREATE (r)-[:ROUND_IN]->(g)";
                executeQuery(session, relationQuery, parameters("roundId", roundId, "gameId", gameId));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void importMovesFromCSV(Session session, String path) {
        try (CSVReader reader = new CSVReader(new FileReader(path + "Moves.csv"))) {
            List<String[]> lines = reader.readAll();
            for (int i = 1; i < lines.size(); i++) {
                String[] line = lines.get(i);
                long gameId = Long.parseLong(line[1]);
                int roundId = Integer.parseInt(line[2]);
                long playerId = Long.parseLong(line[3]);
                int row = Integer.parseInt(line[4]);
                int column = Integer.parseInt(line[5]);
                int cardNumber = Integer.parseInt(line[6]);
                String cardColor = line[7];
                boolean isAllowed = line[8].equals("1");
                String created = line[9];
                String last_updated = line[10];

                String query = "MATCH (p:Player {playerId: $playerId}), (r:Round {roundId: $roundId, gameId: $gameId}) CREATE (p)-[:PLAYS]->(m:Move {row: $row, column: $column, cardColor: $cardColor, cardNumber: $cardNumber, isAllowed: $isAllowed, created: apoc.date.parse($created, 'ms', 'yyyy-MM-dd HH:mm:ss'), last_updated: apoc.date.parse($last_updated, 'ms', 'yyyy-MM-dd HH:mm:ss')})-[:MOVE_IN]->(r)";
                executeQuery(session, query, parameters("playerId", playerId, "gameId", gameId, "roundId", roundId, "row", row, "column", column, "cardColor", cardColor, "cardNumber", cardNumber, "isAllowed", isAllowed, "created", created, "last_updated", last_updated));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void importRoundWinnersFromCSV(Session session, String path) {
        try (CSVReader reader = new CSVReader(new FileReader(path + "RoundWinners.csv"))) {
            List<String[]> lines = reader.readAll();
            // Skip the header row
            for (int i = 1; i < lines.size(); i++) {
                String[] line = lines.get(i);
                long gameId = Long.parseLong(line[0]);
                int roundId = Integer.parseInt(line[1]);
                long playerId = Long.parseLong(line[2]);

                String query = "MATCH (p:Player {playerId: $playerId}), (r:Round {roundId: $roundId, gameId: $gameId}) CREATE (p)-[:WON_ROUND]->(r)";
                executeQuery(session, query, parameters("playerId", playerId, "roundId", roundId, "gameId", gameId));

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void importGameWinnersFromCSV(Session session, String path) {
        try (CSVReader reader = new CSVReader(new FileReader(path + "GameWinners.csv"))) {
            List<String[]> lines = reader.readAll();
            // Skip the header row
            for (int i = 1; i < lines.size(); i++) {
                String[] line = lines.get(i);
                long gameId = Long.parseLong(line[0]);
                long playerId = Long.parseLong(line[1]);

                String query = "MATCH (p:Player {playerId: $playerId}), (g:Game {gameId: $gameId}) CREATE (p)-[:WON_GAME]->(g)";
                executeQuery(session, query, parameters("playerId", playerId, "gameId", gameId));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void importPlayersGamesFromCSV(Session session, String path) {
        try (CSVReader reader = new CSVReader(new FileReader(path + "PlayersGames.csv"))) {
            List<String[]> lines = reader.readAll();
            // Skip the header row
            for (int i = 1; i < lines.size(); i++) {
                String[] line = lines.get(i);
                long gameId = Long.parseLong(line[1]);
                long playerId = Long.parseLong(line[2]);

                String query = "MATCH (p:Player {playerId: $playerId}), (g:Game {gameId: $gameId}) CREATE (p)-[:PLAYS_IN]->(g)";
                executeQuery(session, query, parameters("playerId", playerId, "gameId", gameId));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private String convertTimestampToSqlDate(String timestamp) {
        java.sql.Timestamp ts = new java.sql.Timestamp(Long.parseLong(timestamp));
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return sdf.format(ts);
    }

}
