package univ.ubs.punto.database.mongodb;

public class MongoDbExceptions extends Exception {
    public MongoDbExceptions(String message, Exception e) {
        super(message);
    }

    public static class InsertException extends MongoDbExceptions {
        public InsertException(String message, Exception e) {
            super(message, e);
        }
    }

    public static class SelectException extends MongoDbExceptions {
        public SelectException(String message, Exception e) {
            super(message, e);
        }
    }

    public static class UpdateException extends MongoDbExceptions {
        public UpdateException(String message, Exception e) {
            super(message, e);
        }
    }

    public static class DeleteException extends MongoDbExceptions {
        public DeleteException(String message, Exception e) {
            super(message, e);
        }
    }
}
