package univ.ubs.punto.database.sqlite;

public class SqLiteException extends Exception {
    public SqLiteException(String message, Throwable cause) {
        super(message, cause);
    }

    public static class InsertException extends SqLiteException {
        public InsertException(String message, Exception e) {
            super(message, e);
        }
    }

    public static class SelectException extends SqLiteException {
        public SelectException(String message, Exception e) {
            super(message, e);
        }
    }

    public static class UpdateException extends SqLiteException {
        public UpdateException(String message, Exception e) {
            super(message, e);
        }
    }

    public static class DeleteException extends SqLiteException {
        public DeleteException(String message, Exception e) {
            super(message, e);
        }
    }
}
