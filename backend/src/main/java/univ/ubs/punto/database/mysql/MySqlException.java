package univ.ubs.punto.database.mysql;

public class MySqlException extends Exception {

    public MySqlException(String message, Exception e) {
        super(message, e);
    }

    public static class InsertException extends MySqlException {
        public InsertException(String message, Exception e) {
            super(message, e);
        }
    }

    public static class SelectException extends MySqlException {
        public SelectException(String message, Exception e) {
            super(message, e);
        }
    }

    public static class UpdateException extends MySqlException {
        public UpdateException(String message, Exception e) {
            super(message, e);
        }
    }

    public static class DeleteException extends MySqlException {
        public DeleteException(String message, Exception e) {
            super(message, e);
        }
    }
}