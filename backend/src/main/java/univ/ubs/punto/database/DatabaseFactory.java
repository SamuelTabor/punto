package univ.ubs.punto.database;

import org.json.JSONArray;
import org.json.JSONObject;
import univ.ubs.punto.Config;
import univ.ubs.punto.database.mysql.MySqlConnector;
import univ.ubs.punto.database.neo4j.Neo4jConnector;
import univ.ubs.punto.database.sqlite.SQLiteConnector;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.SQLException;
import java.util.stream.Stream;

public class DatabaseFactory {

    public static DatabaseConnector getInstance() {
        return getInstance(Config.DATABASE);
    }

    public static DatabaseConnector getInstance(Database db) {
        switch (db) {
            case MYSQL:
                return MySqlConnector.getInstance();
            case SQLITE:
                try {
                    return SQLiteConnector.getInstance();
                } catch (SQLException e) {
                    throw new RuntimeException(e);
                }
            case NEO4J:
                return Neo4jConnector.getInstance();
            /*case MONGODB:
                return MongoDbConnector.getInstance();*/
            default:
                throw new IllegalArgumentException("Unsupported database defined in Config.DATABASE.");
        }
    }


    public static void swapTo(Database database) {
        if (database != Config.DATABASE) {
            //Generate csv from current database
            getInstance().generateCSV();

            //Swap to new db
            Config.DATABASE = database;
            try {
                //dump new database
                getInstance().dump();
                //Load data from csv

                getInstance().load(Config.WORKING_DIRECTORY + "/databases/csv/");
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        }
    }




    public static String listAllSaves() throws IOException {
        JSONObject allSaves = new JSONObject();

        for (Database db : Database.values()) {
            if (db != Database.MONGODB) allSaves.put(db.toString(), listSaves(db));
        }

        return allSaves.toString();
    }

    private static JSONArray listSaves(Database db) throws IOException {
        String backupDirectory = switch (db) {
            case MYSQL -> MySqlConnector.BACKUP_PATH;
            case SQLITE -> SQLiteConnector.BACKUP_PATH;
            default -> throw new IllegalArgumentException("Unsupported database: " + db);
        }; // Remplacez par le répertoire de sauvegarde pour la base de données

        try (Stream<Path> paths = Files.list(Paths.get(backupDirectory))) {
            JSONArray saves = new JSONArray();
            paths.map(Path::getFileName)
                    .map(Path::toString)
                    .forEach(saves::put);
            return saves;
        }
    }
}
