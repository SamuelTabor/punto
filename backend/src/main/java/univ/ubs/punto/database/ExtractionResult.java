package univ.ubs.punto.database;

import univ.ubs.punto.model.Game;
import univ.ubs.punto.model.Player;

import java.util.ArrayList;

public class ExtractionResult {
    private ArrayList<Player> players;
    private ArrayList<Game> games;

    public ExtractionResult(ArrayList<Player> players, ArrayList<Game> games) {
        this.players = players;
        this.games = games;
    }

    public ArrayList<Player> getPlayers() {
        return players;
    }

    public ArrayList<Game> getGames() {
        return games;
    }
}