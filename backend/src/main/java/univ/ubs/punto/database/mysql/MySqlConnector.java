package univ.ubs.punto.database.mysql;

import com.opencsv.CSVReader;
import com.opencsv.CSVReaderBuilder;
import com.opencsv.exceptions.CsvValidationException;
import univ.ubs.punto.Config;
import univ.ubs.punto.database.DatabaseConnector;
import univ.ubs.punto.database.ExtractionResult;
import univ.ubs.punto.model.Board;
import univ.ubs.punto.model.Game;
import univ.ubs.punto.model.Player;
import univ.ubs.punto.model.Round;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.*;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Properties;

public class MySqlConnector implements DatabaseConnector {

    private static MySqlConnector instance;
    //todo: Check the url
    private static final String DATABASE_URL = "jdbc:mysql://localhost:3306/PUNTO";
    private static final String USERNAME = "root";
    private static final String PASSWORD = "r5a10punto";
    public static final String BACKUP_PATH = Config.WORKING_DIRECTORY + "/databases/MySql/save/";
    private static final String CSV_PATH = Config.WORKING_DIRECTORY + "/databases/csv/";

    //****************************************************************************************************************//
    //**********************************************  Constructors  *************************************************//

    public static MySqlConnector getInstance() {
        if (instance == null) {
            instance = new MySqlConnector();
        }
        return instance;
    }

    //****************************************************************************************************************//
    //**********************************************  Connection  ****************************************************//

    private Connection getConnection() throws SQLException {
        return DriverManager.getConnection(DATABASE_URL, USERNAME, PASSWORD);
    }

    //****************************************************************************************************************//
    //**********************************************  CRUD for Player  ***********************************************//


    @Override
    public void insert(Player player) throws MySqlException.InsertException {
        String querry = "INSERT INTO Players (id, nom, gameScore, roundScore) VALUES (?, ?, ?, ?)";
        try (PreparedStatement preparedStatement = getConnection().prepareStatement(querry)) {

            preparedStatement.setLong(1, player.getPlayerId());
            preparedStatement.setString(2, player.getNom());
            preparedStatement.setInt(3, player.getGameScore());
            preparedStatement.setInt(4, player.getRoundScore());

            preparedStatement.executeUpdate();

        } catch (SQLException e) {
            throw new MySqlException.InsertException("Error while inserting player in database.", e);
        }
    }


    @Override
    public void update(Player player) throws MySqlException.UpdateException {
        String querry = "UPDATE Players SET nom=?, gameScore=?, roundScore=? WHERE id=?";
        try (PreparedStatement preparedStatement = getConnection().prepareStatement(querry)) {

            preparedStatement.setString(1, player.getNom());
            preparedStatement.setInt(2, player.getGameScore());
            preparedStatement.setInt(3, player.getRoundScore());
            preparedStatement.setLong(4, player.getPlayerId());

            preparedStatement.executeUpdate();

        } catch (SQLException e) {
            throw new MySqlException.UpdateException("Error while updating player in database.", e);
        }
    }


    @Override
    public void delete(Player player) throws MySqlException.DeleteException {
        String querry = "DELETE FROM Players WHERE id=?";
        try (PreparedStatement preparedStatement = getConnection().prepareStatement(querry)) {

            preparedStatement.setLong(1, player.getPlayerId());

            preparedStatement.executeUpdate();

        } catch (SQLException e) {
            throw new MySqlException.DeleteException("Error while deleting player in database.", e);
        }
    }


    @Override
    public Player select(String id) {
        //todo
        return null;
    }

    private boolean playerExists(Player player) throws Exception {
        String query = "SELECT COUNT(*) FROM Players WHERE id = ?";
        try (PreparedStatement preparedStatement = getConnection().prepareStatement(query)) {
            preparedStatement.setLong(1, player.getPlayerId());
            ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                int count = resultSet.getInt(1);
                return count > 0;
            } else {
                return false;
            }
        }
    }

    //****************************************************************************************************************//
    //**********************************************  CRUD for Game  *************************************************//


    @Override
    public void insert(Game game) throws MySqlException.InsertException {
        String querry = "INSERT INTO Games (id, description, nbRounds, gameStatus, currentRound, flagModifiable) VALUES (?, ?, ?, ?, ?, ?)";
        try (PreparedStatement preparedStatement = getConnection().prepareStatement(querry)) {

            preparedStatement.setLong(1, game.getGameId());
            preparedStatement.setString(2, game.getGameDescription());
            preparedStatement.setInt(3, game.getNbRounds());
            preparedStatement.setString(4, game.getGameStatus().toString());
            preparedStatement.setInt(5, game.getCurrentRound().getRoundId());
            preparedStatement.setBoolean(6, game.getFlagModifiable());

            preparedStatement.executeUpdate();

            if (game.getPlayers() != null && !game.getPlayers().isEmpty()) {
                for (Player player : game.getPlayers()) {
                    if (!playerExists(player)) {
                        insert(player);
                    }
                    insertPlayerGame(game.getGameId(), player.getPlayerId());
                }
            }

            if (game.getGameWinners() != null && !game.getGameWinners().isEmpty()) {
                for (Player player : game.getGameWinners()) {
                    insertGameWinner(game.getGameId(), player.getPlayerId());
                }
            }

            if (game.getRounds() != null && !game.getRounds().isEmpty()) {
                for (Round round : game.getRounds()) {
                    insert(round);
                }
            }

        } catch (Exception e) {
            throw new MySqlException.InsertException("Error while inserting game in database.", e);
        }
    }


    @Override
    public void update(Game game) throws MySqlException.UpdateException {
        String querry = "UPDATE Games SET description=?, nbRounds=?, gameStatus=?, currentRound=?, flagModifiable=? WHERE id=?";
        try (PreparedStatement preparedStatement = getConnection().prepareStatement(querry)) {

            preparedStatement.setString(1, game.getGameDescription());
            preparedStatement.setInt(2, game.getNbRounds());
            preparedStatement.setString(3, game.getGameStatus().toString());
            preparedStatement.setInt(4, game.getCurrentRound().getRoundId());
            preparedStatement.setBoolean(5, game.getFlagModifiable());
            preparedStatement.setLong(6, game.getGameId());

            preparedStatement.executeUpdate();

            if (game.getRounds() != null && !game.getRounds().isEmpty()) {
                for (Round round : game.getRounds()) {
                    update(round);
                }
            }

            if (game.getGameWinners() != null && !game.getGameWinners().isEmpty()) {
                for (Player player : game.getGameWinners()) {
                    insertGameWinner(game.getGameId(), player.getPlayerId());
                }
            }

        } catch (SQLException | MySqlException.InsertException e) {
            throw new MySqlException.UpdateException("Error while updating game in database.", e);
        }
    }


    @Override
    public void delete(Game game) throws MySqlException.DeleteException {
        String querry = "DELETE FROM Games WHERE id=?";
        try (PreparedStatement preparedStatement = getConnection().prepareStatement(querry)) {

            preparedStatement.setLong(1, game.getGameId());

            preparedStatement.executeUpdate();

        } catch (SQLException e) {
            throw new MySqlException.DeleteException("Error while deleting game in database.", e);
        }
    }

    @Override
    public Game select(long id) {
        String SELECT_GAME = "SELECT * FROM Games WHERE id=?";
        //TODO
        return null;
    }


    //****************************************************************************************************************//
    //**********************************************  CRUD for Round  ************************************************//

    @Override
    public void insert(Round round) throws MySqlException.InsertException {
        String querry = "INSERT INTO Rounds (id, gameId, roundStatus, currentRoundNum, flagModifiable) VALUES (?, ?, ?, ?, ?)";
        try (PreparedStatement preparedStatement = getConnection().prepareStatement(querry)) {

            preparedStatement.setInt(1, round.getRoundId());
            preparedStatement.setLong(2, round.getGameId());
            preparedStatement.setString(3, round.getRoundStatus().toString());
            preparedStatement.setInt(4, round.getNumRound());
            preparedStatement.setBoolean(5, round.getFlagModifiable());

            preparedStatement.executeUpdate();

        } catch (SQLException e) {
            throw new MySqlException.InsertException("Error while inserting round in database.", e);
        }
    }

    @Override
    public void update(Round round) throws MySqlException.UpdateException {
        String querry = "UPDATE Rounds SET roundStatus=?, currentRoundNum=?, flagModifiable=? WHERE id=?";
        try (PreparedStatement preparedStatement = getConnection().prepareStatement(querry)) {

            preparedStatement.setString(1, round.getRoundStatus().toString());
            preparedStatement.setInt(2, round.getNumRound());
            preparedStatement.setBoolean(3, round.getFlagModifiable());
            preparedStatement.setInt(4, round.getRoundId());

            preparedStatement.executeUpdate();

            if (round.getWinners() != null && !round.getWinners().isEmpty()) {
                for (Player player : round.getWinners()) {
                    if (!isWinnerAlreadyAdded(round.getGameId(), round.getRoundId(), player.getPlayerId())) {
                        insertRoundWinner(round.getGameId(), round.getRoundId(), player.getPlayerId());
                    }
                }
            }

        } catch (SQLException | MySqlException e) {
            throw new MySqlException.UpdateException("Error while updating round in database.", e);
        }
    }


    @Override
    public void delete(Round round) throws MySqlException.DeleteException {
        String querry = "DELETE FROM Rounds WHERE id=?";
        try (PreparedStatement preparedStatement = getConnection().prepareStatement(querry)) {

            preparedStatement.setInt(1, round.getRoundId());

            preparedStatement.executeUpdate();

        } catch (SQLException e) {
            throw new MySqlException.DeleteException("Error while deleting round in database.", e);
        }
    }


    @Override
    public Round select(int id) {
        //todo
        return null;
    }

    private boolean isWinnerAlreadyAdded(long gameId, int roundId, long playerId) throws MySqlException {
        String query = "SELECT * FROM RoundWinners WHERE gameId=? AND roundId=? AND playerId=?";
        try (PreparedStatement preparedStatement = getConnection().prepareStatement(query)) {
            preparedStatement.setLong(1, gameId);
            preparedStatement.setInt(2, roundId);
            preparedStatement.setLong(3, playerId);

            ResultSet resultSet = preparedStatement.executeQuery();
            return resultSet.next(); // retourne true si le gagnant est déjà dans la table, false sinon
        } catch (SQLException e) {
            throw new MySqlException("Error while checking if winner is already added in database.", e);
        }
    }

    //****************************************************************************************************************//
    //*********************************************  Insert for Move  ************************************************//

    @Override
    public void insert(long gameId, int roundId, long playerId, int row, int column, String cardColor, int cardNumber, boolean allowed) throws MySqlException.InsertException {
        String querry = "INSERT INTO Moves (gameId, roundId, playerId, rowIndex, colIndex, cardNumber, cardColor, allowed) VALUES (?, ?, ?, ?, ?, ?, ?, ?)";
        try (PreparedStatement preparedStatement = getConnection().prepareStatement(querry)) {

            preparedStatement.setLong(1, gameId);
            preparedStatement.setInt(2, roundId);
            preparedStatement.setLong(3, playerId);
            preparedStatement.setInt(4, row);
            preparedStatement.setInt(5, column);
            preparedStatement.setInt(6, cardNumber);
            preparedStatement.setString(7, cardColor);
            preparedStatement.setBoolean(8, allowed);

            preparedStatement.executeUpdate();

        } catch (SQLException e) {
            throw new MySqlException.InsertException("Error while inserting move in database.", e);
        }
    }

    //****************************************************************************************************************//
    //**********************************************  CRUD for GameWinner  *******************************************//

    private void insertGameWinner(long gameId, long playerId) throws MySqlException.InsertException {
        String querry = "INSERT INTO GameWinners (gameId, playerId) VALUES (?, ?)";
        try (PreparedStatement preparedStatement = getConnection().prepareStatement(querry)) {

            preparedStatement.setLong(1, gameId);
            preparedStatement.setLong(2, playerId);

            preparedStatement.executeUpdate();

        } catch (SQLException e) {
            throw new MySqlException.InsertException("Error while inserting game winner in database.", e);
        }
    }

    private void deleteGameWinner(long gameId, long playerId) throws MySqlException.DeleteException {
        String querry = "DELETE FROM GameWinners WHERE gameId=? AND playerId=?";
        try (PreparedStatement preparedStatement = getConnection().prepareStatement(querry)) {

            preparedStatement.setLong(1, gameId);
            preparedStatement.setLong(2, playerId);

            preparedStatement.executeUpdate();

        } catch (SQLException e) {
            throw new MySqlException.DeleteException("Error while deleting game winner in database.", e);
        }
    }

    private ArrayList<Player> selectGameWinner(long gameId) {
        //todo
        return null;
    }

    //****************************************************************************************************************//
    //**********************************************  CRUD for RoundWinner  ******************************************//

    private void insertRoundWinner(long gameId, int roundId, long playerId) throws MySqlException.InsertException {
        String querry = "INSERT INTO RoundWinners (gameId, roundId, playerId) VALUES (?, ?, ?)";
        try (PreparedStatement preparedStatement = getConnection().prepareStatement(querry)) {

            preparedStatement.setLong(1, gameId);
            preparedStatement.setInt(2, roundId);
            preparedStatement.setLong(3, playerId);

            preparedStatement.executeUpdate();

        } catch (SQLException e) {
            throw new MySqlException.InsertException("Error while inserting round winner in database.", e);
        }
    }

    private void deleteRoundWinner(long gameId, int roundId, long playerId) throws MySqlException.DeleteException {
        String querry = "DELETE FROM RoundWinners WHERE gameId=? AND roundId=? AND playerId=?";
        try (PreparedStatement preparedStatement = getConnection().prepareStatement(querry)) {

            preparedStatement.setLong(1, gameId);
            preparedStatement.setInt(2, roundId);
            preparedStatement.setLong(3, playerId);

            preparedStatement.executeUpdate();

        } catch (SQLException e) {
            throw new MySqlException.DeleteException("Error while deleting round winner in database.", e);
        }
    }

    private ArrayList<Player> selectRoundWinner(long gameId, int roundId) {
        //todo
        return null;
    }

    //****************************************************************************************************************//
    //**********************************************  CRUD for PlayerGame  *******************************************//

    private void insertPlayerGame(long gameId, long playerId) throws MySqlException.InsertException {
        String querry = "INSERT INTO PlayersGames (gameId, playerId) VALUES (?, ?)";
        try (PreparedStatement preparedStatement = getConnection().prepareStatement(querry)) {

            preparedStatement.setLong(1, gameId);
            preparedStatement.setLong(2, playerId);

            preparedStatement.executeUpdate();

        } catch (SQLException e) {
            throw new MySqlException.InsertException("Error while inserting player game in database.", e);
        }
    }

    private void deletePlayerGame(long gameId, long playerId) {
        String querry = "DELETE FROM PlayersGames WHERE gameId=? AND playerId=?";
        try (PreparedStatement preparedStatement = getConnection().prepareStatement(querry)) {

            preparedStatement.setLong(1, gameId);
            preparedStatement.setLong(2, playerId);

            preparedStatement.executeUpdate();

        } catch (SQLException e) {
            try {
                throw new MySqlException.DeleteException("Error while deleting player game in database.", e);
            } catch (MySqlException.DeleteException ex) {
                throw new RuntimeException(ex);
            }
        }
    }

    private ArrayList<Player> selectPlayerGame(long gameId) {
        //todo
        return null;
    }

    //****************************************************************************************************************//
    //**********************************************  Dump && Extract  ***********************************************//


    @Override
    public void dump() throws MySqlException {
        try (Statement statement = getConnection().createStatement()) {

            statement.executeUpdate("DELETE FROM Moves");
            statement.executeUpdate("DELETE FROM Boards");
            statement.executeUpdate("DELETE FROM RoundWinners");
            statement.executeUpdate("DELETE FROM Rounds");
            statement.executeUpdate("DELETE FROM GameWinners");
            statement.executeUpdate("DELETE FROM PlayersGames");
            statement.executeUpdate("DELETE FROM Games");
            statement.executeUpdate("DELETE FROM Players");


        } catch (SQLException e) {
            throw new MySqlException("Error while dumping database.", e);
        }
    }

    @Override
    public void save() {
        List<String> tables = Arrays.asList("Players", "Games", "Rounds", "PlayersGames", "GameWinners", "RoundWinners", "Moves");

        // Obtenez la date actuelle pour créer un dossier de sauvegarde unique
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd_HH-mm-ss");
        String backupDate = dateFormat.format(new Date(System.currentTimeMillis()));

        // Créez le nom du dossier avec le préfixe "backup_date_mysql"
        String backupFolderName = "backup_" + backupDate + "_mysql";

        // Obtenez le chemin complet du dossier de sauvegarde
        String backupFolderPath = BACKUP_PATH + backupFolderName;

        // Créez le dossier de sauvegarde
        File backupFolder = new File(backupFolderPath);
        if (!backupFolder.exists()) {
            backupFolder.mkdirs(); // Créez le dossier et ses parents s'ils n'existent pas déjà
        }

        // Générez les fichiers CSV pour chaque table dans le dossier de sauvegarde
        for (String tableName : tables) {
            generateTableCSV(tableName, backupFolderPath);
        }

        System.out.println("Tables saved successfully to backup folder: " + backupFolderPath);
    }

    @Override
    public void generateCSV() {
        List<String> tables = Arrays.asList("Players", "Games", "Rounds", "PlayersGames", "GameWinners", "RoundWinners", "Moves");


        for (String tableName : tables) {
            generateTableCSV(tableName, CSV_PATH); // Spécifiez le chemin du dossier où vous souhaitez sauvegarder les fichiers
        }
    }

    @Override
    public void load(String folderPath) {
        System.out.println(folderPath);

        try {
            // Établir la connexion à la base de données SQLite
            Connection connection = getConnection();

            // Obtenir la liste des fichiers CSV dans le dossier
            List<String> tables = Arrays.asList("Players", "Games", "Rounds", "PlayersGames", "GameWinners", "RoundWinners", "Moves");

            // Parcourir chaque table dans l'ordre spécifié
            for (String tableName : tables) {
                // Construire le chemin complet du fichier CSV
                String filePath = folderPath + "/" + tableName + ".csv";

                // Charger le fichier CSV dans la base de données
                loadCsvFile(connection, tableName, Paths.get(filePath));
            }

            // Fermer la connexion à la base de données
            connection.close();

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private void loadCsvFile(Connection connection, String tableName, Path filePath) {
        try {
            // Utiliser OpenCSV pour lire le fichier CSV
            try (CSVReader csvReader = new CSVReaderBuilder(new FileReader(filePath.toFile())).build()) {
                // Lire la première ligne pour obtenir les noms de colonnes
                String[] columnNames = csvReader.readNext();

                // Vérifier si la table existe, sinon la créer
                createTableIfNotExists(connection, tableName, columnNames);

                // Créer une déclaration SQL préparée pour l'insertion
                String insertQuery = createInsertQuery(tableName, columnNames);
                PreparedStatement preparedStatement = connection.prepareStatement(insertQuery);

                // Utiliser OpenCSV pour lire les lignes suivantes du fichier CSV
                String[] values;
                while ((values = csvReader.readNext()) != null) {
                    // Vérifier si le nombre de valeurs correspond au nombre de colonnes
                    if (values.length == columnNames.length) {
                        // Ajouter les valeurs du CSV à la déclaration SQL préparée
                        for (int i = 0; i < values.length; i++) {
                            preparedStatement.setString(i + 1, values[i]);
                        }

                        // Exécuter la requête d'insertion
                        preparedStatement.executeUpdate();
                    } else {
                        System.out.println("La ligne du CSV ne correspond pas au nombre de colonnes.");
                    }
                }

                // Fermer la déclaration SQL préparée
                preparedStatement.close();
            }

            System.out.println("Table '" + tableName + "' loaded successfully.");

        } catch (IOException | SQLException | CsvValidationException e) {
            e.printStackTrace();
        }
    }

    private void createTableIfNotExists(Connection connection, String tableName, String[] columnNames) throws SQLException {
        // Logique de création de table à adapter en fonction de vos besoins
        // Cette logique suppose que tous les types de données sont des chaînes (TEXT)
        // Vous devrez adapter cette logique pour gérer différents types de données
        StringBuilder createTableQuery = new StringBuilder("CREATE TABLE IF NOT EXISTS ")
                .append(tableName)
                .append(" (");

        for (String columnName : columnNames) {
            createTableQuery.append(columnName).append(" TEXT, ");
        }

        // Supprimer la virgule finale et fermer la parenthèse
        createTableQuery.delete(createTableQuery.length() - 2, createTableQuery.length()).append(")");

        // Exécuter la requête de création de table
        try (PreparedStatement createTableStatement = connection.prepareStatement(createTableQuery.toString())) {
            createTableStatement.executeUpdate();
        }
    }

    // Méthode pour créer la requête d'insertion en fonction des colonnes
    private String createInsertQuery(String tableName, String[] columnNames) {
        // Logique de création de requête d'insertion à adapter en fonction de vos besoins
        StringBuilder insertQuery = new StringBuilder("INSERT INTO ")
                .append(tableName)
                .append(" (");

        for (String columnName : columnNames) {
            insertQuery.append(columnName).append(", ");
        }

        // Supprimer la virgule finale et ajouter les paramètres de requête préparée
        insertQuery.delete(insertQuery.length() - 2, insertQuery.length()).append(") VALUES (");

        for (int i = 0; i < columnNames.length; i++) {
            insertQuery.append("?, ");
        }

        // Supprimer la virgule finale et fermer la parenthèse
        insertQuery.delete(insertQuery.length() - 2, insertQuery.length()).append(")");

        return insertQuery.toString();
    }

    // Méthode pour générer un fichier CSV à partir d'une table spécifique
    private void generateTableCSV(String tableName, String folderPath) {
        Connection connection = null;
        Statement statement = null;
        ResultSet resultSet = null;
        PrintWriter writer = null;

        try {
            // Établir la connexion à la base de données
            connection = getConnection();

            // Créer une déclaration SQL
            statement = connection.createStatement();

            // Exécuter la requête pour récupérer toutes les lignes de la table
            resultSet = statement.executeQuery("SELECT * FROM " + tableName);

            // Construire le chemin complet du fichier CSV
            String filePath = folderPath + "/" + tableName + ".csv";

            // Ouvrir le fichier CSV pour l'écriture
            writer = new PrintWriter(new FileWriter(filePath));

            // Écrire l'en-tête du CSV avec les noms de colonnes
            for (int i = 1; i <= resultSet.getMetaData().getColumnCount(); i++) {
                writer.print(resultSet.getMetaData().getColumnName(i));
                if (i < resultSet.getMetaData().getColumnCount()) {
                    writer.print(",");
                }
            }
            writer.println(); // Saut de ligne après l'en-tête

            // Écrire les données de la table dans le fichier CSV
            while (resultSet.next()) {
                for (int i = 1; i <= resultSet.getMetaData().getColumnCount(); i++) {
                    writer.print(resultSet.getString(i));
                    if (i < resultSet.getMetaData().getColumnCount()) {
                        writer.print(",");
                    }
                }
                writer.println(); // Saut de ligne après chaque ligne de données
            }

            System.out.println("CSV file generated successfully: " + filePath);

        } catch (SQLException | IOException e) {
            e.printStackTrace();
        } finally {
            // Fermer les ressources
            try {
                if (resultSet != null) resultSet.close();
                if (statement != null) statement.close();
                if (connection != null) connection.close();
                if (writer != null) writer.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }


}