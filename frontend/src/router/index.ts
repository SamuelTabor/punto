import { createRouter, createWebHistory, RouteRecordRaw } from "vue-router";
import CreateGameView from "@views/CreateGameView.vue";

const routes: Array<RouteRecordRaw> = [
  {
    path: "/",
    name: "create game",
    component:   CreateGameView,
  },
  {
    path: "/test",
    name: "test",
    component: () =>
      import("@views/TestView.vue")
  },
  {
    path: "/play",
    name: "play",
    component: () =>
      import("@views/PlayView.vue")
  },
  {
    path: "/admin",
    name: "admin",
    component: () =>
        import("@views/AdminView.vue")
  },

];

const router = createRouter({
  history: createWebHistory(),
  routes,
});

export default router;
