export enum EnumStatus {
  WAITING = "WAITING",
  STARTED = "STARTED",
  FINISHED = "FINISHED"
}