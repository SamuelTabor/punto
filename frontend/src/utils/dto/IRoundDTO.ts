import { EnumStatus } from '@/utils/enums/EnumStatus';
import { IPlayerDTO } from '@/utils/dto/IPlayerDTO';
import { ICardDTO } from '@/utils/dto/ICardDTO';
import {ICellDTO} from "@/utils/dto/ICellDTO";

export interface IRoundDTO {
  gameID: number;
  roundId: number;
  roundNumber: number;
  status: EnumStatus;
  winners: IPlayerDTO[];
  currentPlayer: IPlayerDTO | null;
  currentCard: ICardDTO | null;
  board: ICellDTO[][];
}
