import { ICardDTO } from '@/utils/dto/ICardDTO';

export interface ICellDTO {
  row: number;
  column: number;
  card: ICardDTO | null;
  isPlayable: boolean;
}