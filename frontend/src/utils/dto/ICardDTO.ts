import { EnumColor } from '@/utils/enums/EnumColor';

export interface ICardDTO {
  cardNumber: number;
  cardColor: EnumColor;
}