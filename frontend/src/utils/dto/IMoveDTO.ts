import {ICardDTO} from "@/utils/dto/ICardDTO";

export interface IMoveDTO {
    playerId: number;
    roundId: number;
    row: number;
    column: number;
    card: ICardDTO;
}