import { IPlayerDTO } from "@/utils/dto/IPlayerDTO";
import { EnumStatus } from "@/utils/enums/EnumStatus";
import { IRoundDTO } from "@/utils/dto/IRoundDTO";

export interface IGameDTO {
  gameId: number | null;
  nbRounds: number;
  players: IPlayerDTO[];
  description: string;
  status: EnumStatus | null;
  winners: IPlayerDTO[] | null;
  currentRound: number;
  allRounds: [{
    roundId: number;
    numberOfRounds: number;
    status: EnumStatus;
    winners: IPlayerDTO[];
  }];
}
