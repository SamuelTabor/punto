import { createStore } from "vuex";
import createPersistedState from "vuex-persistedstate";
import { IPlayerDTO } from '@/utils/dto/IPlayerDTO';
import {IGameDTO} from "@/utils/dto/IGameDTO";
import {IRoundDTO} from "@/utils/dto/IRoundDTO";


export default createStore({
  state: {
    players: <{ player: IPlayerDTO; isToggled: boolean }[]>[],
    game: <IGameDTO>{},
    lastRound: <IRoundDTO>{},
    currentRound: <number>0,
  },
  getters: {
    getPlayers: (state) => state.players,
    getGame: (state) => state.game,
    getCurrentRound: (state) => state.currentRound
  },
  mutations: {
    addPlayer(state, player: IPlayerDTO) {
      state.players.push({ player, isToggled: false });
    },
    setPlayers(state, players: IPlayerDTO[]) {
      state.players = players.map((player) => ({ player, isToggled: false }));
    },
    setGame(state, game: IGameDTO) {
      // Ajoutez cette mutation
      state.game = game;
    },
    togglePlayer(state, playerId: number) {
      const player = state.players.find((p) => p.player.playerId === playerId);
      if (player) {
        player.isToggled = !player.isToggled;
      }
    },
    setCurrentRound(state, round: number) {
      state.currentRound = round;
    },
  },
  actions: {
    addPlayer({commit}, player: IPlayerDTO) {
      commit("addPlayer", player);
    },
    setPlayers({commit}, players: IPlayerDTO[]) {
      // Ajoutez cette action
      commit("setPlayers", players);
    },
    setGame({commit}, game: IGameDTO) {
      // Ajoutez cette action
      commit("setGame", game);
    },
  },
  modules: {},
  plugins: [createPersistedState({
    paths: ["players", "game"],
  })],
});
